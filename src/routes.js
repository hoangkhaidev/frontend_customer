import Category from "./view/Category/Category";
import Customers from "./view/Customers/Customers";
import Order from "./view/Order/Order";
import OrderDetail from "./view/Order/OrderDetail";
import Products from "./view/Products/Products";

const routes = [
    {
      path: "/product",
      exact: true,
      component: Products
    },
    {
      path: "/customer",
      exact: true,
      component: Customers
    },
    {
      path: "/category",
      exact: true,
      component: Category
    },
    {
      path: "/order",
      exact: true,
      component: Order,
      routes: [
        {
          path: "/order/view/:id",
          component: OrderDetail
        }
      ]
    }
];
export default routes;