import firebase from "firebase/app";
import "firebase/firestore";
import 'firebase/storage';

const firebaseConfig = {
    apiKey: "AIzaSyANDmxrd0ISUBvyJTIelWQ5HGhHQt-p6cI",
    authDomain: "clouddb-82a1d.firebaseapp.com",
    projectId: "clouddb-82a1d",
    storageBucket: "clouddb-82a1d.appspot.com",
    messagingSenderId: "74993365111",
    appId: "1:74993365111:web:88e946c5f22c5d817f5672",
    measurementId: "G-4MBVRFYKXJ"
};
firebase.initializeApp(firebaseConfig);
export default firebase;