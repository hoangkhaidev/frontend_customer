/* eslint-disable import/no-anonymous-default-export */
import axios from './axiosClient';

const signIn = async(data) => {
  const token = await axios.post('/auth/sign-in', data.user);
  return token;
};

export default { signIn };
