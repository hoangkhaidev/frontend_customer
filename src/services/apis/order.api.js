/* eslint-disable import/no-anonymous-default-export */
import axios from './axiosClient';

const getOrder = async () => {
  const order =  await axios.get(`/order`);
  return order;
};

const getOrderFirst = async (orderId) => {
  const orderFirst = await axios.get(`/order/${orderId}`);
  return orderFirst;
};

const deleteOrder = async (orderId) => {
   return await axios.delete(`/order/${orderId}`);
};

export default {
  getOrder,
  deleteOrder,
  getOrderFirst
};
