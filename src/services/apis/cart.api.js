/* eslint-disable import/no-anonymous-default-export */
import fakeApi from '../../utils/fakeApi';
import axios from './axiosClient';

let dataCart = JSON.parse(localStorage.getItem('cart')); 
let row = dataCart ? dataCart : [];

const createOrder = async (order) => {
  const orders = await axios.post('/order', order);
  return orders;
};

const createOrderDetail = async (data) => {
  const orders = await axios.post(`/order_detail/${data.orderId}`, data.dataCart);
  return orders;
}

const getCart = async () => {
   return await fakeApi(true, [...row], 0);
};

const updateCart = async (CartID, Cart) => {
//   return await axios.patch(`/Cart/${CartID}`, Cart);
};

const deleteCart = async (CartId) => {
//    return await axios.delete(`/Cart/${CartId}`);
};

export default {
  createOrder,
  getCart,
  updateCart,
  deleteCart,
  createOrderDetail
};
