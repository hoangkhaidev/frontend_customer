import axios from './axiosClient';

const createProduct = async (product) => {
  const product_first = await axios.post('/product/', product);
  return product_first;
};

const createProductCategory = async (data) => {
  const category_product = await axios.post(`/category_product/${data.categoryId}/product/${data.productId}`);
  return category_product;
};

const createImage = async (data) => {
  const image = await axios.post(`/image/${data.productId}`, data.image);
  console.log(image);
  return image;
};

const getProducts = async () => {
  const productList =  await axios.get(`/Product`);
  return productList;
};

const getProduct = async (productId) => {
};

const updateProduct = async (productId, product) => {
  return await axios.patch(`/product/${productId}`, product);
};

const updateCategoryProduct = async (data) => {
  console.log(data);
  return await axios.patch(`/category_product/${data.category_ProductId}/category/${data.categoryId}/product/${data.productId}`);
};

const updateImage = async (data) => {
  console.log(data)
  return await axios.patch(`/image/${data.imageId}/product/${data.productId}`, data.image);
}

const deleteProduct = async (productId) => {
  return await axios.delete(`/product/${productId}`);
};

const deleteCategoryProduct = async (category_ProductId) => {
  return await axios.delete(`/category_product/${category_ProductId}`);
};

const deleteImage = async (imageId) => {
  return await axios.delete(`/image/${imageId}`);
};
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  createProduct,
  getProducts,
  getProduct,
  updateProduct,
  deleteProduct,
  createProductCategory,
  deleteCategoryProduct,
  updateCategoryProduct,
  createImage,
  updateImage,
  deleteImage,
};
