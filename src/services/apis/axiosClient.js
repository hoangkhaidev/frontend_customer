import axios from 'axios';
import queryString from 'query-string';

const instance = axios.create({
  // eslint-disable-next-line no-undef
  baseURL: process.env.REACT_APP_BASE_URL,
  headers: {
    'content-type': 'application/json',
  },
  paramsSerializer: params => queryString.stringify(params),
});

instance.interceptors.request.use(
  (config) => {
    const token = "localStorage.getItem('accessToken')";
    if (
      config.headers['Authorization'] === null ||
      config.headers['Authorization'] === '' ||
      config.headers['Authorization'] === undefined
    ) {
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }
    }
    return config;
  },
  (error) => {
    console.log(error);
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  (response) => {
    //Any status code that lie within the rage of 2xx cause this function to trigger
    //Do something with response data
    if (response && response.data){
      return response.data;
    }
    return response;
  },
  (error) => {
    console.log(error);
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);
export default instance;
