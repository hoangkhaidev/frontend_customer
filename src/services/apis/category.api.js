/* eslint-disable import/no-anonymous-default-export */
import axios from './axiosClient';

const createCategory = async (Category) => {
  const Category_first = await axios.post('/Category', Category);
  return Category_first;
};

const getCategory = async () => {
  const Category =  await axios.get(`/Category`);
  return Category;
};

const updateCategory = async (CategoryID, Category) => {
  return await axios.patch(`/Category/${CategoryID}`, Category);
};

const deleteCategory = async (CategoryId) => {
   return await axios.delete(`/Category/${CategoryId}`);
};

export default {
  createCategory,
  getCategory,
  updateCategory,
  deleteCategory
};
