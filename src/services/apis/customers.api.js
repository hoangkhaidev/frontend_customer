/* eslint-disable import/no-anonymous-default-export */
import axios from './axiosClient';

const createCustomer = async (customer) => {
  const customer_first = await axios.post('/customer', customer);
  return customer_first;
};

const getCustomers = async () => {
  const customer =  await axios.get(`/customer`);
  return customer;
};

const getCustomer = async (customerId) => {
};

const updateCustomer = async (customerID, customer) => {
  return await axios.patch(`/customer/${customerID}`, customer);
};

const deleteCustomer = async (customerId) => {
  return await axios.delete(`/customer/${customerId}`);
};

export default {
  createCustomer,
  getCustomers,
  getCustomer,
  updateCustomer,
  deleteCustomer
};
