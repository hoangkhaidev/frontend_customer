/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Divider,
  TextField,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {},
  dialogForm: {
    overflow: 'unset !important'
  },
  dialogContent: {
    overflow: 'unset !important',
  },
  subTitle: {
    textAlign: 'center'
  },
  text: {
    textAlign: 'center',
    marginTop: '10px',
    color: '#d9d9d9',
    fontSize: '14px'
  }
}));

const Payment = (props) => {
  // eslint-disable-next-line
  const {
    openCreate,
    handleClosePayment,
    handleSubmitPayment,
    cart,
    totalPrice,
    ...rest
  } = props;

  const classes = useStyles();

  //* init form value

  const [formState, setFormState] = useState({
    fullName: '',
    tel: '',
    address: ''
  });
  //validate
  const [errors, setErrors] = useState({});
  const handleValidation = () => {
    let errors = {};
    let formIsValid = true;
    const re = /^[0-9\b]+$/;
    
    if(!formState.fullName.trim()){
      formIsValid = false;
      errors.fullName = 'Full name cannot be empty';
    }
    
    if(!formState.address.trim()){
      formIsValid = false;
      errors.address = 'address cannot be empty';
    }

    if(!formState.tel.trim()){
      formIsValid = false;
      errors.tel = 'Tel cannot be empty';
    }
    
    if(!re.test(formState.tel)){
      formIsValid = false;
      errors.tel = 'Tel only number';
    }
    if(!totalPrice){
      formIsValid = false;
      errors.totalPrice = 'No products in the order';
    }
    setErrors(errors);
    return formIsValid;
  }

  const handleSubmit = () => {
    if (handleValidation()) {
      handleSubmitPayment(formState, cart, totalPrice);
      handleClosePayment();
    }
  }

  //* init create form value
  useEffect(() => {
    setFormState({
      fullName: '',
      tel: '',
      address: ''
    });
    // eslint-disable-next-line
  }, [openCreate]);

  //* handle reset form
  const handleReset = () => {
    setFormState({
      fullName: '',
      tel: '',
      address: ''
    });
    handleClosePayment();
  };

  //* set form state when change form value
  const handleChange = (event) => {
    setFormState({
      ...formState,
      [event.target.name]:
        event.target.type === 'checkbox'
          ? event.target.checked :
          event.target.type === 'type'
            ? event.target.files[0].name :
            event.target.value.trim()
    });
  };

  //* UI
  return (
    <>
      <Grid item sm={12} xs={12}>
        <Dialog
          aria-labelledby="form-dialog-title"
          className={classes.dialogForm}
          open={openCreate}
        >
          <DialogTitle id="form-dialog-title">Payment</DialogTitle>

          <Divider />

          <DialogContent className={classes.dialogContent}>
            <Grid container spacing={2}>
              <Grid item sm={12} xs={12}>
                <Grid container spacing={2}>
                  <Grid className={classes.name} item sm={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Full Name"
                      name="fullName"
                      onChange={handleChange}
                      required
                      variant="outlined"
                      margin="dense"
                    />
                    <span style={{color: "red"}}>{errors.fullName}</span>
                  </Grid>
                  <Grid className={classes.tel} item sm={6} xs={12}>
                    <TextField
                      fullWidth
                      label="Phone Number"
                      name="tel"
                      onChange={handleChange}
                      required
                      variant="outlined"
                      margin="dense"
                    />
                    <span style={{color: "red"}}>{errors.tel}</span>
                  </Grid>
                  <Grid className={classes.address} item sm={6} xs={12}>
                    <TextField
                      fullWidth
                      label="Address"
                      name="address"
                      onChange={handleChange}
                      required
                      variant="outlined"
                      margin="dense"
                    />
                    <span style={{color: "red"}}>{errors.address}</span>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>

          <Divider />

          <DialogActions>
            <span style={{color: "red"}}>{errors.totalPrice}</span>
            <button
              content="Create new"
              onClick={handleSubmit}
              type="blue-full"
              color="primary"
              className={classes.button}
              variant="contained"
            >
              Save
            </button>
            <button
              content="Cancel"
              onClick={handleReset}
              type="gray-full"
              variant="contained"
              color="secondary"
              className={classes.button}
            >
              Cancel
            </button>
          </DialogActions>
        </Dialog>
      </Grid>
    </>
  );
};

export default Payment;

Payment.propTypes = {
  openCreate: PropTypes.bool,
  handleClosePayment: PropTypes.func,
  handleSubmitPayment: PropTypes.func,
};