/* eslint-disable react-hooks/exhaustive-deps */
import numeral from 'numeral';
import React, { useState } from 'react';
import Payment from './components/Payment/Payment';

function Cart(props) {
    const { cart, onHandleDelete, onHandleUpdateQuantity, addOrderPayment } = props;

    const showTotalAmount = (cart) => {
        var totalPrice = 0;
        if(cart.length > 0) {
            for ( let i = 0; i < cart.length; i++ ){
                totalPrice += cart[i].quantity * cart[i].product.price;
            }
        }
        return totalPrice;
    }

    const onDelete = (cartItem) => {
        onHandleDelete(cartItem);
    }

    const onUpdateQuantity = (product, quantity) => {
        if (quantity > 0) {
            onHandleUpdateQuantity(product, quantity);
        }
    }

	// payment
    const [ openCreate, setOpenCreate ] = useState(false);

    const showCreate = () => {
        setOpenCreate(true);
    }

    const closeCreate = () => {
        setOpenCreate(false);
    }

    const handleClosePayment = () => {
        closeCreate();
    }

    const handleSubmitPayment = (order, carts, totalPrice) => {
        addOrderPayment(order, carts, totalPrice);
    }

    return (
		<>
            <section className="section">
                <div className="table-responsive">
                    <table className="table product-table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Sản Phẩm</th>
                                <th>Giá</th>
                                <th>Số Lượng</th>
                                <th>Tổng Cộng</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {cart.map((item, index) => (
                                <tr key={index}>
                                    <th scope="row">
                                        <img style={{maxWidth: '150px'}} src={item.product.images.length ? item.product.images.map((image) => (image.thumbnail)) : 'https://cidrapbusiness.org/wp-content/uploads/2017/10/noimage.gif'} alt="" className="img-fluid z-depth-0" />
                                    </th>
                                    <td style={{verticalAlign: 'middle', textAlign: 'left'}}>
                                        <h5>
                                            <strong>{item.product.name}</strong>
                                        </h5>
                                    </td>
                                    <td style={{verticalAlign: 'middle', textAlign: 'left'}}> {numeral(item.product.price).format('0,0')} đ</td>
                                    <td style={{verticalAlign: 'middle', textAlign: 'left'}} className="center-on-small-only">
                                        <span className="qty"> {item.quantity} </span>
                                        <div className="btn-group radio-group" data-toggle="buttons">
                                            <label 
                                                className="btn btn-sm btn-primary btn-rounded waves-effect waves-light"
                                                onClick={ () => { onUpdateQuantity(item.product, item.quantity - 1) } }
                                            >
                                                <button 
                                                    style={{color: '#fff',textDecoration: 'none',background: 'none', border: 'none'}}
                                                >—</button>
                                            </label>
                                            <label 
                                                onClick={ () => { onUpdateQuantity(item.product, item.quantity + 1)  }}
                                                className="btn btn-sm btn-primary btn-rounded waves-effect waves-light"
                                            >
                                                <button 
                                                    style={{color: '#fff',textDecoration: 'none', background: 'none', border: 'none'}}
                                                >+</button>
                                            </label>
                                        </div>
                                    </td>
                                    <td style={{verticalAlign: 'middle', textAlign: 'left'}}>{numeral(item.product.price * item.quantity).format('0,0')} đ</td>
                                    <td style={{verticalAlign: 'middle', textAlign: 'left'}}>
                                        <button
                                            type="button" 
                                            className="btn btn-sm btn-primary waves-effect waves-light" 
                                            data-toggle="tooltip" data-placement="top" 
                                            title="" data-original-title="Remove item"
                                            onClick = { () => { onDelete(item) } }
                                        >
                                            X
                                        </button>
                                    </td>
                                </tr>
                            ))}
                            <tr>
                                <td colSpan="3"></td>
                                <td>
                                    <h4>
                                        <strong>Tổng Tiền</strong>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <strong>
                                            {numeral(showTotalAmount(cart)).format('0,0') } đ
                                        </strong>
                                    </h4>
                                </td>
                                <td colSpan="3">
                                    <button 
                                        type="button" 
                                        className="btn btn-primary waves-effect waves-light"
                                        onClick={() => showCreate()}
                                    >Thanh toán
                                        <i className="fa fa-angle-right right"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
            <Payment 
                openCreate={openCreate} 
                handleClosePayment={handleClosePayment} 
                handleSubmitPayment={handleSubmitPayment} 
                cart={cart}
                totalPrice={showTotalAmount(cart)}
            />
        </>
    );
}
export default Cart;