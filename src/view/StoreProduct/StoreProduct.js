/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useDispatch, useSelector } from 'react-redux';
import { getProducts } from '../../store/asyncActions/product.asyncAction';
import numeral from 'numeral';
import Cart from './Cart';
import { clearStateProduct } from '../../store/slices/Product.slice';
import { clearStateCart } from '../../store/slices/Cart.slice';
import { createCart, deleteCart, getCart, updateCart, addPayment } from '../../store/asyncActions/cart.asyncAction';

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '90%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

function StoreProduct() {
    const classes = useStyles();
    const dispatch = useDispatch();
    let cart = useSelector((state) => state.cart.cart);

    let productList = useSelector((state) => state.products.products);

    const addCart = (product) => {
        getDispatchCreateCart(product);
    }

    const getDispatchCreateCart = (product) => {
        dispatch (
            createCart({
                product: product,
                quantity: 1
            })
        )
    }

    const getDispatchProducts = () => {
        dispatch (
            getProducts()
        )
    }
    
    const getDispatchGetCart = () => {
        dispatch (
            getCart()
        )
    }
    
    //delete cart
    const onHandleDelete = (cartItem) => {
        getDispatchDeleteCart(cartItem);
    }

    const getDispatchDeleteCart = (cartItem) => {
        dispatch (
            deleteCart({
                cartItem
            })
        )
    }
    //update cart
    const onHandleUpdateQuantity = (product, quantity) => {
        getDispatchUpdateCart(product, quantity);
    }

    const getDispatchUpdateCart = (product, quantity) => {
        dispatch (
            updateCart({
                product: product,
                quantity: quantity
            })
        )
    }

    const clearDispatchProducts = () => {
		dispatch (
			clearStateProduct()
		)
	}

    const clearDispatchCart = () => {
		dispatch (
			clearStateCart()
		)
	}
    //payment
    const addOrderPayment = (order, carts, totalPrice) => {
        getDispatchAddPayment(order, carts, totalPrice)
    }

    const getDispatchAddPayment = (order, carts, totalPrice) => {
        dispatch (
            addPayment({
                order: order,
                carts: carts,
                totalPrice: totalPrice
            })
        )
    }

    useEffect(() => {
        getDispatchProducts();
        getDispatchGetCart();
    }, []);

    useEffect(() => {
        return () => {
            clearDispatchProducts();
            clearDispatchCart();
        }
    }, []);

    return (
        <main>
            {/* Hero unit */}
            <div className={classes.heroContent}>
            <Container maxWidth="sm">
                <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                    Danh sách sản phẩm
                </Typography>
            </Container>
            </div>
            <Container className={classes.cardGrid} maxWidth="md">
            {/* End hero unit */}
                <Grid container spacing={4}>
                    {productList.map((item, index) => (
                        <Grid item key={index} xs={12} sm={6} md={4}>
                            <Card className={classes.card}>
                            <CardMedia
                                className={classes.cardMedia}
                                image={item.images.length ? item.images.map((image) => image.thumbnail) : 'https://cidrapbusiness.org/wp-content/uploads/2017/10/noimage.gif'}
                                title="Image title"
                            />
                            <CardContent className={classes.cardContent}>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {item.name}
                                </Typography>
                                <Typography>
                                    {item.description}
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button 
                                    size="small" 
                                    color="primary" 
                                    onClick={() => addCart(item) }
                                >
                                    Add Cart
                                </Button>
                                <Button size="small" color="primary">
                                    {numeral(item.price).format('0,0')} đ
                                </Button>
                            </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>
            <Cart 
                cart={cart}
                onHandleDelete={onHandleDelete}
                onHandleUpdateQuantity={onHandleUpdateQuantity}
                addOrderPayment={addOrderPayment}
            />
        </main>
    );
}
export default StoreProduct;
