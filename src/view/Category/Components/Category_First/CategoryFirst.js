/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {  useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import UpdateCategory from '../UpdateCategory/UpdateCategory';

const useStyles = makeStyles(() => ({
    table: {
        minWidth: 650,
    },
    th_table: {
        fontSize: '15px',
        textAlign: 'left'
    },
    img_style: {
        width: '150px',
        borderRadius: '50%'
    }

}));

function CategoryFirst(props) {
    const classes = useStyles();
    let { deleteCategoryDispatch, updateCategoryDispatch } = props;
    const { category, stt } = props;
    
    const onDeleteCategory = (id) => {
        deleteCategoryDispatch(id);
    }

    const onUpdateCategory = (id, category) => {
        handleOpenUpdateCategory();
    }

	const [openUpdate, setOpenUpdate] = useState(false);
    const handleOpenUpdateCategory = () => setOpenUpdate(true);
	const handleCloseUpdateCategory = () => setOpenUpdate(false);
	const handleSubmitUpdateCategory = (categoryId, category) => updateCategoryDispatch(categoryId, category);

    return (
        <>
            <TableRow>
                <TableCell className={classes.th_table} component="th" scope="row">{stt}</TableCell>
                <TableCell className={classes.th_table} align="right">{category.name}</TableCell>
                <TableCell className={classes.th_table} align="right">
                    <button type="button" className="btn btn-primary" onClick={() => onUpdateCategory(category.id, category)} style={{ marginRight: '5px' }}>Sửa</button>
                    <button type="button" className="btn btn-danger" onClick={() => onDeleteCategory(category.id)}>Xóa</button>
                </TableCell>
            </TableRow>
            <UpdateCategory
                category={category}
                handleCloseUpdateCategory={handleCloseUpdateCategory}
                handleSubmitUpdateCategory={handleSubmitUpdateCategory}
                openUpdate={openUpdate}
            />
        </>
    );
}

export default CategoryFirst;