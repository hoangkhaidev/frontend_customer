/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Divider,
  TextField,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {},
  dialogForm: {
    overflow: 'unset !important'
  },
  dialogContent: {
    overflow: 'unset !important',
  },
  subTitle: {
    textAlign: 'center'
  },
  text: {
    textAlign: 'center',
    marginTop: '10px',
    color: '#d9d9d9',
    fontSize: '14px'
  }
}));

const UpdateCategory = (props) => {
  // eslint-disable-next-line
  const { openUpdate, category, handleCloseUpdateCategory, handleSubmitUpdateCategory, ...rest } = props;

  const classes = useStyles();

  //* init form value
  const [formState, setFormState] = useState({
    name: '',
  });
 
  const handleSubmit = () => {
    console.log(category.id);
    handleSubmitUpdateCategory(category.id, formState);
    handleCloseUpdateCategory();
  };

  //* handle reset form
  const handleReset = () => {
    setFormState({
      name: '',
    });

    handleCloseUpdateCategory();
  };

  //* set form state when change form value
  const handleChange = (event) => {
    setFormState({
      ...formState,
      [event.target.name]:
        event.target.type === 'checkbox'
          ? event.target.checked
          : event.target.value
    });
  };

  //* init update form value
  useEffect(() => {
    setFormState({
      name: category.name,
    });
  }, [category, openUpdate]);

  //* UI
  return (
    <>
      <Grid item sm={12} xs={12}>
        <Dialog
          aria-labelledby="form-dialog-title"
          className={classes.dialogForm}
          open={openUpdate}
        >
          <DialogTitle id="form-dialog-title">Update Category</DialogTitle>

          <Divider />

          <DialogContent className={classes.dialogContent}>
            <Grid container spacing={2}>
              <Grid item sm={12} xs={12}>
                <Grid container spacing={2}>
                  <Grid className={classes.name} item sm={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Full Name"
                      name="name"
                      onChange={handleChange}
                      required
                      value={formState.name}
                      variant="outlined"
                      margin="dense"
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>

          <Divider />

          <DialogActions>
            <button
              content="Create new"
              onClick={handleSubmit}
              type="blue-full"
              color="primary"
              className={classes.button}
              variant="contained"
            >
              Save
            </button>
            <button
              content="Cancel"
              onClick={handleReset}
              type="gray-full"
              variant="contained"
              color="secondary"
              className={classes.button}
            >
              Cancel
            </button>
          </DialogActions>
        </Dialog>
      </Grid>
     
    </>
  );
};

export default UpdateCategory;

UpdateCategory.propTypes = {
  category: PropTypes.object,
  handleCloseUpdateCategory: PropTypes.func,
  handleSubmitUpdateCategory: PropTypes.func,
  openUpdate: PropTypes.bool
};
