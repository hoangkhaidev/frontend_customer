/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { createCategory, deleteCategory, getCategory, updateCategory } from '../../store/asyncActions/category.asyncAction';
import CreateCategory from './Components/CreateCategory/CreateCategory';
import CategoryFirst from './Components/Category_First/CategoryFirst';
import { clearStateCategory } from '../../store/slices/Category.slice';

const useStyles = makeStyles(() => ({
	table: {
		minWidth: 650,
	},
	th_table: {
		fontSize: '15px',
		textAlign: 'left'
	},
	img_style:{
		width: '150px',
    	borderRadius: '50%'
	},
	loading_table:{
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		display: 'flex',
		zIndex: 3,
		position: 'fixed',
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: '30px',
	}
}));

function Category() {
	const classes = useStyles();
	const dispatch = useDispatch();

	let rows = useSelector((state) => state.category.category);

	const [ openCreate, setOpenCreate ] = useState(false);
	// create
    const showCreate = () => {
        setOpenCreate(true);
    }

    const closeCreate = () => {
        setOpenCreate(false);
    }

    const handleCloseCreateCategory = () => {
        closeCreate();
    }

	const handleSubmitCreateCategory = (category) => {
        createCategoryDispatch(category);
        closeCreate();
    }

	const createCategoryDispatch = (category) => {
		dispatch(
			createCategory({
				category: category
			})
		)
	}

	//delete
	const deleteCategoryDispatch = (categoryID) => {
		dispatch(
			deleteCategory({
				categoryID
			})
		)
	}
	// update
	const updateCategoryDispatch = (categoryID, category) => {
		dispatch(
			updateCategory({
				categoryID,
				category
			})
		)
	}

	useEffect(()=>{
		dispatch(getCategory());
	}, []);

	useEffect(() => {
		return () => {
		  dispatch(clearStateCategory()); //* clear state when unmount
		};
	}, []);
	return (
		<>
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label="simple table">
					<TableHead >
						<TableRow>
							<TableCell className={classes.th_table} >STT</TableCell>
							<TableCell className={classes.th_table} >Tên</TableCell>
							<TableCell className={classes.th_table} align="right">
								<button type="button" className="btn btn-success" onClick={() => showCreate()}>Thêm</button>
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{ rows.map((category, index) => (
							<CategoryFirst 
								key={index}
								stt={index + 1}
								category={category}
								deleteCategoryDispatch={deleteCategoryDispatch} 
								updateCategoryDispatch={updateCategoryDispatch}
							/>
						)) }
					</TableBody>
				</Table>
			</TableContainer>
			<CreateCategory 
                openCreate={openCreate} 
                setOpenCreate={setOpenCreate} 
                handleCloseCreateCategory={handleCloseCreateCategory}
                handleSubmitCreateCategory={handleSubmitCreateCategory}
            />
		</>

	);
}

export default Category;
