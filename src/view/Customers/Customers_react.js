/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { getCustomers } from '../../store/asyncActions/customer.asyncAction';
import { clearStateCustomer } from '../../store/slices/customerSlice';
import Customer from './components/Customer/Customer';
import CreateCustomer from './components/CreateCustomer/CreateCustomer';

const useStyles = makeStyles(() => ({
	table: {
		minWidth: 650,
		
	},
	th_table: {
		fontSize: '15px',
		textAlign: 'left'
	},
	img_style:{
		width: '150px',
    	borderRadius: '50%'
	}

}));

function Customers() {
	const classes = useStyles();
	const dispatch = useDispatch();

	const rows = useSelector((state) => state.customers.customers);
	const [openCreate, setOpenCreate] = false;

	const showCreate = () => {
		setOpenCreate(true);
	}

	const closeCreate = () => {
		setOpenCreate(false);
	}

	const handleCloseCreateCustomer = () => {
		closeCreate();
	}

	const handleSubmitCreateCustomer = (data) => {
		closeCreate();
	}

	useEffect(()=>{
		dispatch(getCustomers());
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		return () => {
		  dispatch(clearStateCustomer()); //* clear state when unmount
		};
	  }, []);

	return (
		<>
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell className={classes.th_table} >STT</TableCell>
							<TableCell className={classes.th_table} >Tên Nhân Viên</TableCell>
							<TableCell className={classes.th_table} align="right">Hình Ảnh</TableCell>
							<TableCell className={classes.th_table} align="right">Email</TableCell>
							<TableCell className={classes.th_table} align="right">Số Điện Thoại</TableCell>
							<TableCell className={classes.th_table} align="right">Địa Chỉ</TableCell>
							<TableCell className={classes.th_table} align="right">
								<button type="button" className="btn btn-success" onClick={()=>showCreate()}>Thêm</button>
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{rows.map((row, index) => (
							<Customer key={ index } row={row} />
						))}
					</TableBody>
				</Table>
			</TableContainer>

			<CreateCustomer 
				openCreate={openCreate}
				handleCloseCreateCustomer={handleCloseCreateCustomer}
				handleSubmitCreateCustomer={handleSubmitCreateCustomer}
			/>
		</>
	);
}

export default Customers;