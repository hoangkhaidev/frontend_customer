/* eslint-disable jsx-a11y/img-redundant-alt */
import React, {  useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import UpdateCustomer from '../UpdateCustomer/UpdateCustomer';

const useStyles = makeStyles(() => ({
    table: {
        minWidth: 650,
    },
    th_table: {
        fontSize: '15px',
        textAlign: 'left'
    },
    img_style: {
        width: '150px',
        borderRadius: '50%'
    }

}));

function Customer(props) {
    const classes = useStyles();
    let { deleteCustomerDispatch, updateCustomerDispatch } = props;
    const { customer } = props;
    
    const onDeleteCustomer = (id) => {
        deleteCustomerDispatch(id);
    }

    const onUpdateCustomer = (id, customer) => {
        handleOpenUpdateCustomer();
    }

	const [openUpdate, setOpenUpdate] = useState(false);
    const handleOpenUpdateCustomer = () => setOpenUpdate(true);
	const handleCloseUpdateCustomer = () => setOpenUpdate(false);
	const handleSubmitUpdateCustomer = (customerId, customer) => updateCustomerDispatch(customerId, customer);

    return (
        <>
            <TableRow>
                <TableCell className={classes.th_table} component="th" scope="row">{customer.id}</TableCell>
                <TableCell className={classes.th_table} align="right">{customer.name}</TableCell>
                <TableCell className={classes.th_table} align="right">{customer.tel}</TableCell>
                <TableCell className={classes.th_table} align="right">{customer.address}</TableCell>
                <TableCell className={classes.th_table} align="right">
                    <button type="button" className="btn btn-primary" onClick={ () => onUpdateCustomer(customer.id, customer) } style={{ marginRight: '5px' }}>Sửa</button>
                    <button type="button" className="btn btn-danger" onClick={ () => {
                        onDeleteCustomer(customer.id)}
                    }>Xóa</button>
                </TableCell>
            </TableRow>
            <UpdateCustomer
                customer={customer}
                handleCloseUpdateCustomer={handleCloseUpdateCustomer}
                handleSubmitUpdateCustomer={handleSubmitUpdateCustomer}
                openUpdate={openUpdate}
            />
        </>
    );
}

export default Customer;