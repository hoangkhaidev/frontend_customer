/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Divider,
  TextField,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {},
  dialogForm: {
    overflow: 'unset !important'
  },
  dialogContent: {
    overflow: 'unset !important',
  },
  subTitle: {
    textAlign: 'center'
  },
  text: {
    textAlign: 'center',
    marginTop: '10px',
    color: '#d9d9d9',
    fontSize: '14px'
  }
}));

const UpdateCustomer = (props) => {
  // eslint-disable-next-line
  const { openUpdate, customer, handleCloseUpdateCustomer, handleSubmitUpdateCustomer, ...rest } = props;

  const classes = useStyles();

  //* init form value
  const [formState, setFormState] = useState({
    id: '',
    name: '',
    tel: '',
    address: ''
  });
 
  const handleSubmit = () => {
    console.log(customer.id);
    handleSubmitUpdateCustomer(customer.id, formState);
    handleCloseUpdateCustomer();
  };

  //* handle reset form
  const handleReset = () => {
    setFormState({
      id: '',
      name: '',
      tel: '',
      address: ''
    });

    handleCloseUpdateCustomer();
  };

  //* set form state when change form value
  const handleChange = (event) => {
    setFormState({
      ...formState,
      [event.target.name]:
        event.target.type === 'checkbox'
          ? event.target.checked
          : event.target.value
    });
  };

  //* init update form value
  useEffect(() => {
    setFormState({
      name: customer.name,
      tel: customer.tel,
      address: customer.address
    });
  }, [customer, openUpdate]);

  //* UI
  return (
    <>
      <Grid item sm={12} xs={12}>
        <Dialog
          aria-labelledby="form-dialog-title"
          className={classes.dialogForm}
          open={openUpdate}
        >
          <DialogTitle id="form-dialog-title">Update Customer</DialogTitle>

          <Divider />

          <DialogContent className={classes.dialogContent}>
            <Grid container spacing={2}>
              <Grid item sm={12} xs={12}>
                <Grid container spacing={2}>
                  <Grid className={classes.name} item sm={12} xs={12}>
                    <TextField
                      fullWidth
                      label="Full Name"
                      name="name"
                      onChange={handleChange}
                      required
                      value={formState.name}
                      variant="outlined"
                      margin="dense"
                    />
                  </Grid>
                  <Grid className={classes.tel} item sm={6} xs={12}>
                    <TextField
                      fullWidth
                      label="Phone Number"
                      name="tel"
                      onChange={handleChange}
                      required
                      value={formState.tel}
                      variant="outlined"
                      margin="dense"
                    />
                  </Grid>
                  <Grid className={classes.address} item sm={6} xs={12}>
                    <TextField
                      fullWidth
                      label="Address"
                      name="address"
                      onChange={handleChange}
                      required
                      value={formState.address}
                      variant="outlined"
                      margin="dense"
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>

          <Divider />

          <DialogActions>
            <button
              content="Create new"
              onClick={handleSubmit}
              type="blue-full"
              color="primary"
              className={classes.button}
              variant="contained"
            >
              Save
            </button>
            <button
              content="Cancel"
              onClick={handleReset}
              type="gray-full"
              variant="contained"
              color="secondary"
              className={classes.button}
            >
              Cancel
            </button>
          </DialogActions>
        </Dialog>
      </Grid>

    </>
  );
};

export default UpdateCustomer;

UpdateCustomer.propTypes = {
  customer: PropTypes.object,
  handleCloseUpdateCustomer: PropTypes.func,
  handleSubmitUpdateCustomer: PropTypes.func,
  openUpdate: PropTypes.bool
};