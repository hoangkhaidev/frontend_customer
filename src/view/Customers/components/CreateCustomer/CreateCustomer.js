/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Divider,
  TextField,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Select,
  Button
} from '@material-ui/core';
import validate from 'validate.js';
// import { button, DialogConfirm, TextFormat } from 'components';

const useStyles = makeStyles(() => ({
  root: {},
  dialogForm: {
    overflow: 'unset !important'
  },
  dialogContent: {
    overflow: 'unset !important',
  },
  subTitle: {
    textAlign: 'center'
  },
  text: {
    textAlign: 'center',
    marginTop: '10px',
    color: '#d9d9d9',
    fontSize: '14px'
  }
}));

const schema = {
  name: {
    presence: { allowEmpty: false, message: 'is required' } 
  },
  tel: {
    presence: { allowEmpty: false, message: 'is required' },
    format: {
      pattern: "^[0-9\b]+$",
      flags: "i",
      message: "is not a phone number"
    }
  },
  address: {
    presence: { allowEmpty: false, message: 'is required' }
  },
}


const CreateCustomer = (props) => {
  // eslint-disable-next-line
  const {
    openCreate,
    handleCloseCreateCustomer,
    handleSubmitCreateCustomer,
    ...rest
  } = props;

  const classes = useStyles();

  //* init form value

  const [formState, setFormState] = useState({
    isValid: false,
    values: {
      id: '',
      name: '',
      tel: '',
      address: ''
    },
    errors: {},
    touched: {}
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    if (formState.isValid === true) {
      handleSubmitCreateCustomer(formState.values);
    }
  }

  //* init create form value
  useEffect(() => {
    setFormState({
      isValid: false,
      values: {
        id: '',
        name: '',
        tel: '',
        address: ''
      },
      errors: {},
      touched: {}
    });
    // eslint-disable-next-line
  }, [openCreate]);

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState((formState) => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  //* handle reset form
  const handleReset = () => {
    setFormState({
      isValid: false,
      values: {
        id: '',
        name: '',
        tel: '',
        address: ''
      },
      errors: {},
      touched: {}
    });
    handleCloseCreateCustomer();
  };

  //* set form state when change form value
  const handleChange = (event) => {
    event.persist();
    
    setFormState((formState) => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };
  //* check error form
  console.log(formState.errors);
  const hasError = (field) => formState.touched[field] && formState.errors[field] ? true : false;
  //* UI
  return (
    <>
      <Grid item sm={12} xs={12}>
        <Dialog
          aria-labelledby="form-dialog-title"
          className={classes.dialogForm}
          open={openCreate}
        >
          <DialogTitle id="form-dialog-title">Create Customer</DialogTitle>
          <form onSubmit={handleSubmit}>
            <Divider />

            <DialogContent className={classes.dialogContent}>
              <Grid container spacing={2}>
                <Grid item sm={12} xs={12}>
                  <Grid container spacing={2}>
                    <Grid className={classes.name} item sm={12} xs={12}>
                      <TextField
                        fullWidth
                        label="Full Name"
                        name="name"
                        error={hasError('name')}
                        onChange={handleChange}
                        helperText={
                          hasError('name') ? formState.errors.name[0] : null
                        }
                        required
                        variant="outlined"
                        margin="dense"
                      />
                    </Grid>
                    <Grid className={classes.tel} item sm={6} xs={12}>
                      <TextField
                        fullWidth
                        label="Phone Number"
                        name="tel"
                        error={hasError('tel')}
                        onChange={handleChange}
                        helperText={
                          hasError('tel') ? formState.errors.tel[0] : null 
                        }
                        required
                        variant="outlined"
                        margin="dense"
                      />
                    </Grid>
                    <Grid className={classes.address} item sm={6} xs={12}>
                      <TextField
                        fullWidth
                        label="Address"
                        name="address"
                        error={hasError('address')}
                        onChange={handleChange}
                        helperText={
                          hasError('address') ? formState.errors.address[0] : null
                        }
                        required
                        variant="outlined"
                        margin="dense"
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </DialogContent>

            <Divider />

            <DialogActions>
              
              <button
                content="Create new"
                type="submit"
                color="primary"
                className={classes.button}
                variant="contained"
                disabled={!formState.isValid}
              >
                {/* <Save className={classes.icon} /> */}
                Save
              </button>
              <button
                content="Cancel"
                onClick={handleReset}
                type="gray-full"
                variant="contained"
                color="secondary"
                className={classes.button}
              >
                {/* <Cancel className={classes.icon} /> */}
                Cancel
              </button>
            </DialogActions>
          </form>
        </Dialog>
      </Grid>
    </>
  );
};

export default CreateCustomer;

CreateCustomer.propTypes = {
  openCreate: PropTypes.bool,
  handleCloseCreateCustomer: PropTypes.func,
  handleSubmitCreateCustomer: PropTypes.func
};
