/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { createCustomer, deleteCustomer, getCustomers, updateCustomer } from '../../store/asyncActions/customer.asyncAction';
import Customer from './components/Customer/Customer';
import Search from './components/Search/Search';
import CreateCustomer from './components/CreateCustomer/CreateCustomer';
import { clearStateCustomer, mySearch, mySort } from '../../store/slices/Customer.slice';

const useStyles = makeStyles(() => ({
	table: {
		minWidth: 650,
	},
	th_table: {
		fontSize: '15px',
		textAlign: 'left'
	},
	img_style:{
		width: '150px',
    	borderRadius: '50%'
	}

}));

function Customers() {

	const classes = useStyles();
	const dispatch = useDispatch();
	
	let rows = useSelector((state) => state.customers.customers);

	const searchValue = useSelector((state) => state.customers.searchValue);
	if (searchValue) {
		rows = rows.filter((item) => {
			return item.name.toUpperCase().indexOf(searchValue) !== -1;
		});
	}

    const [ openCreate, setOpenCreate ] = useState(false);
	// create
    const showCreate = () => {
        setOpenCreate(true);
    }

    const closeCreate = () => {
        setOpenCreate(false);
    }

    const handleCloseCreateCustomer = () => {
        closeCreate();
    }

    const createCustomerDispatch = (customer) => {
		dispatch(
			createCustomer({
				customer: customer
			})
		)
	}

	const handleSubmitCreateCustomer = (customer) => {
        createCustomerDispatch(customer);
        closeCreate();
    }
	//delete
	const deleteCustomerDispatch = (customerID) => {
		dispatch(
			deleteCustomer({
				customerID
			})
		)
	}

    const onSort = (sortBy) => {
		getCustomersDispatch(sortBy);
	}
	//search
	const onSearch = (searchValue) => {
		getCustomersSearchDispatch(searchValue);
	} 
	// update
	const updateCustomerDispatch = (customerID, customer) => {
		dispatch(
			updateCustomer({
				customerID,
				customer
			})
		)
	}
	//Sort
	const getCustomersDispatch = async (sortBy = '') => {
		dispatch(
			mySort({
					sortBy: sortBy
			})
		);
	}
	//search
	const getCustomersSearchDispatch = async (searchValue = '') => {
		dispatch(
			mySearch({
				searchValue: searchValue
			})
		);
	}
	useEffect(()=>{
		getCustomersSearchDispatch();
		dispatch(getCustomers());
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		return () => {
		  dispatch(clearStateCustomer()); //* clear state when unmount
		};
	}, []);

	return (
		<>
			<Search onSort={onSort} onSearch={onSearch} />
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label="simple table">
					<TableHead>
						<TableRow>
							<TableCell className={classes.th_table} >Mã nhân viên</TableCell>
							<TableCell className={classes.th_table} >Tên Nhân Viên</TableCell>
							<TableCell className={classes.th_table} align="right">Số Điện Thoại</TableCell>
							<TableCell className={classes.th_table} align="right">Địa Chỉ</TableCell>
							<TableCell className={classes.th_table} align="right">
								<button type="button" className="btn btn-success" onClick={ () => showCreate() }>Thêm</button>
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{
							rows.map((customer, index) => (
								<Customer 
									key={ index } 
									customer={customer} 
									deleteCustomerDispatch={deleteCustomerDispatch} 
									updateCustomerDispatch={updateCustomerDispatch}
								/>
							))
						}
					</TableBody>
				</Table>
			</TableContainer>

			<CreateCustomer 
                openCreate={openCreate} 
                setOpenCreate={setOpenCreate} 
                handleCloseCreateCustomer={handleCloseCreateCustomer}
                handleSubmitCreateCustomer={handleSubmitCreateCustomer}
            />
			
		</>
	);
}

export default Customers;