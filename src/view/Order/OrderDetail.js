/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import OrderInfo from './OrderInfo';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import { getOrderFirst } from '../../store/asyncActions/order.asyncAction';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles(() => ({
	table: {
		minWidth: 650,
	},
	th_table: {
		fontSize: '15px',
		textAlign: 'left'
	},
	img_style:{
		width: '150px',
    	borderRadius: '50%'
	},
	loading_table:{
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		display: 'flex',
		zIndex: 3,
		position: 'fixed',
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: '30px',
	}
}));

function OrderDetail(props) {
	const classes = useStyles();
	const dispatch = useDispatch();

    const orderId = props.location.aboutProps;
	console.log(orderId);
	if (orderId) {
		localStorage.setItem('requestId', JSON.stringify(orderId));
	}
	let requestId = JSON.parse(localStorage.getItem('requestId'));
	let row = useSelector((state) => state.order.orderFirst);
	let orderDetails = row.orderDetails ? row.orderDetails : [] ;
	
	useEffect(() => {
		dispatch(
			getOrderFirst({
				requestId
			})
		)
	}, [requestId]);
	
	return (
		<>
			<OrderInfo row={row}/>
			<CardActions>
				<Button style={{fontSize: '14px',fontWeight: '600'}}>Chi tiết đơn hàng:</Button>
			</CardActions>
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label="simple table">
					<TableHead >
						<TableRow>
							<TableCell className={classes.th_table} >STT</TableCell>
							<TableCell className={classes.th_table} >Tên sản phẩm</TableCell>
							<TableCell className={classes.th_table} align="right">Đơn giá</TableCell>
							<TableCell className={classes.th_table} >Số lượng</TableCell>
							<TableCell className={classes.th_table} >Thành tiền</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{ orderDetails.map((item, index) => (
							<TableRow key={index} >
								<TableCell className={classes.th_table} component="th" scope="row">{index + 1}</TableCell>
								<TableCell className={classes.th_table} align="right">{item.product.name}</TableCell>
								<TableCell className={classes.th_table} align="right">{item.product.price}</TableCell>
								<TableCell className={classes.th_table} align="right">{item.quantity}</TableCell>
								<TableCell className={classes.th_table} align="right">{item.product.price * item.quantity}</TableCell>
							</TableRow>
						)) } 
						<TableRow>
							<TableCell className={classes.th_table} align="left" colSpan={3}></TableCell>
							<TableCell className={classes.th_table} align="left" style={{fontWeight: '600'}}>Total:</TableCell>
							<TableCell className={classes.th_table} align="left" style={{fontWeight: '600'}}>{row.totalPrice}</TableCell>
						</TableRow>
					</TableBody>
				</Table>
			</TableContainer>
		</>

	);
}

export default OrderDetail;
