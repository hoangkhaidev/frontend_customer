/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(() => ({
	table: {
		minWidth: 650,
	},
	th_table: {
		fontSize: '15px',
		textAlign: 'left'
	},
	img_style:{
		width: '150px',
    	borderRadius: '50%'
	},
	loading_table:{
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		display: 'flex',
		zIndex: 3,
		position: 'fixed',
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: '30px',
	}
}));

function OrderInfo(props) {
	const classes = useStyles();
    const { row } = props;
    function formatDate(string){
        return new Date(string).toLocaleString();
    }
	return (
		<>
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label="simple table">
					<TableHead >
						<TableRow colSpan={3}>
							<TableCell className={classes.th_table} style={{fontWeight: '600'}}>Họ Tên:</TableCell>
							<TableCell className={classes.th_table} >{row.fullName}</TableCell>
							<TableCell className={classes.th_table} ></TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						<TableRow colSpan={3}>
							<TableCell className={classes.th_table} style={{fontWeight: '600'}}>Số điện thoại:</TableCell>
							<TableCell className={classes.th_table} >{row.tel}</TableCell>
							<TableCell className={classes.th_table} ></TableCell>
						</TableRow>
						<TableRow colSpan={3}>
							<TableCell className={classes.th_table} style={{fontWeight: '600'}}>Địa chỉ:</TableCell>
							<TableCell className={classes.th_table} >{row.address}</TableCell>
							<TableCell className={classes.th_table} ></TableCell>
						</TableRow>
						<TableRow colSpan={3}>
							<TableCell className={classes.th_table} style={{fontWeight: '600'}}>Ngày đặt hàng:</TableCell>
							<TableCell className={classes.th_table} >{formatDate(row.create_date)}</TableCell>
							<TableCell className={classes.th_table} ></TableCell>
						</TableRow>
					</TableBody>
				</Table>
			</TableContainer>
		</>
	);
}

export default OrderInfo;
