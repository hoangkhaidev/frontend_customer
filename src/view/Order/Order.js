/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { deleteOrder, getOrder } from '../../store/asyncActions/order.asyncAction';
import OrderFirst from './Components/Order_First/OrderFirst';
import { clearStateOrder } from '../../store/slices/Order.slice';

const useStyles = makeStyles(() => ({
	table: {
		minWidth: 650,
	},
	th_table: {
		fontSize: '15px',
		textAlign: 'left'
	},
	img_style:{
		width: '150px',
    	borderRadius: '50%'
	},
	loading_table:{
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		display: 'flex',
		zIndex: 3,
		position: 'fixed',
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: '30px',
	}
}));

function Order() {
	const classes = useStyles();
	const dispatch = useDispatch();

	const rows = useSelector((state) => state.order.order);
	//delete
	const deleteOrderDispatch = (orderID) => {
		dispatch(
			deleteOrder({
				orderID
			})
		)
	}

	useEffect(()=>{
		dispatch(getOrder());
	}, []);

	useEffect(() => {
		return () => {
		  dispatch(clearStateOrder()); //* clear state when unmount
		};
	}, []);

	return (
		<>
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label="simple table">
					<TableHead >
						<TableRow>
							<TableCell className={classes.th_table} >STT</TableCell>
							<TableCell className={classes.th_table} >Họ Tên</TableCell>
							<TableCell className={classes.th_table} align="right">Số điện thoại</TableCell>
							<TableCell className={classes.th_table} >Địa chỉ</TableCell>
							<TableCell className={classes.th_table} >Tổng tiền</TableCell>
							<TableCell className={classes.th_table} >Ngày đặt hàng</TableCell>
							<TableCell className={classes.th_table} ></TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{ rows.map((order, index) => (
							<OrderFirst 
								key={index}
								stt={index + 1}
								order={order}
								deleteOrderDispatch={deleteOrderDispatch}
							/>
						)) }
					</TableBody>
				</Table>
			</TableContainer>
		</>

	);
}

export default Order;
