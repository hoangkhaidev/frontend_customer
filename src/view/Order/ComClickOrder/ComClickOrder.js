/* eslint-disable no-unused-vars */
import {
  Dialog,
  DialogActions, DialogTitle, Grid
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import React from 'react';

const useStyles = makeStyles(() => ({
  root: {},
  dialogForm: {
    overflow: 'unset !important'
  },
  dialogContent: {
    overflow: 'unset !important',
  },
  subTitle: {
    textAlign: 'center'
  },
  text: {
    textAlign: 'center',
    marginTop: '10px',
    color: '#d9d9d9',
    fontSize: '14px'
  },

}));

const ComClickOrder = (props) => {
  // eslint-disable-next-line
  const classes = useStyles();

  const { openComClick, onCloseComClick, onDeleteProduct, orderId} = props;
  const onHandleSubmit = () => {
      onCloseComClick();
      onDeleteProduct(orderId);
  }
  const CloseComClick = () => {
      onCloseComClick();
  }
  
  return (
      <Grid item sm={12} xs={12}>
        <Dialog
          aria-labelledby="form-dialog-title"
          className={classes.dialogForm}
          open={openComClick}
        >
          <DialogTitle id="form-dialog-title">Bạn có chắc chắn muốn thay đổi không ?</DialogTitle>
          <DialogActions>
            <button type="button" className="btn btn-default" onClick={() => onHandleSubmit() }>Phải</button>
            <button type="button" className="btn btn-default" onClick={() => CloseComClick() }>Không</button>
          </DialogActions> 
        </Dialog>
      </Grid>
  );
};

export default ComClickOrder;

