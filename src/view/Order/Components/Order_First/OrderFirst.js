/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import ComClickOrder from '../../ComClickOrder/ComClickOrder';
import { NavLink } from 'react-router-dom';

const useStyles = makeStyles(() => ({
    table: {
        minWidth: 650,
    },
    th_table: {
        fontSize: '15px',
        textAlign: 'left'
    },
    img_style: {
        width: '150px',
        borderRadius: '50%'
    },
    button: {
        marginRight: '5px',
        fontSize: '12px',
    },

}));

function OrderFirst(props) {
    const classes = useStyles();
    const { deleteOrderDispatch, order, stt } = props;

    function formatDate(string){
        return new Date(string).toLocaleString();
    }

    //comClick
	const [openComClick, setComClick] = useState(false);
	
    const onOpenComClick = () => {
        setComClick(true);
    }

    const onCloseComClick = () => {
        setComClick(false);
    }

	const onDeleteProduct = (id) => {
		deleteOrderDispatch(id);
    }

    const onDeleteProductBefore = () => {
        onOpenComClick();
    }
    return (
        <>
            <TableRow>
                <TableCell className={classes.th_table} component="th" scope="row">{stt}</TableCell>
                <TableCell className={classes.th_table} align="right">{order.fullName}</TableCell>
                <TableCell className={classes.th_table} align="right">{order.tel}</TableCell>
                <TableCell className={classes.th_table} align="right">{order.address}</TableCell>
                <TableCell className={classes.th_table} align="right">{order.totalPrice}</TableCell>
                <TableCell className={classes.th_table} align="right">{formatDate(order.create_date)}</TableCell>
                <TableCell className={classes.th_table} align="right">
                    <NavLink 
                        to={{
                            pathname: `/order/${stt}`,
                            aboutProps: order.id,
                        }}
                        style={{color:'#fff',}}
                    >
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            endIcon={<VisibilityIcon></VisibilityIcon>}
                            style={{fontSize: '13px'}}
                        >
                            
                            Xem
                        </Button>
                    </NavLink>
                    <Button
                        variant="contained"
                        color="secondary"
                        className={classes.button}
                        startIcon={<DeleteIcon />}
                        onClick={() => onDeleteProductBefore()}
                    >
                        Xóa
                    </Button>
                    <ComClickOrder
                        openComClick={openComClick}
                        onCloseComClick={onCloseComClick}
                        onDeleteProduct={onDeleteProduct}
                        orderId={order.id}
                    />
                </TableCell>
            </TableRow>
            
        </>
    );
}

export default OrderFirst;