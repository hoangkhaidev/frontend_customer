/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/alt-text */
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles } from '@material-ui/styles';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createProduct, deleteProduct, getProducts, updateProduct } from '../../store/asyncActions/product.asyncAction';
import { clearStateProduct, searchStateProduct, sortStateProduct, stateLimit } from '../../store/slices/Product.slice';
import loadingImg from './../../image/loading.gif';
import Message from './Alert/Message';
import CreateProduct from './CreateProduct/CreateProduct';
import Product from './Product';
import SearchProduct from './SearchProduct';

const useStyles = makeStyles(() => ({
	table: {
		minWidth: 650,
	},
	th_table: {
		fontSize: '15px',
		textAlign: 'left'
	},
	img_style:{
		width: '150px',
    	borderRadius: '50%'
	},
	loading_table:{
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		display: 'flex',
		zIndex: 3,
		position: 'fixed',
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: '30px',
	}
}));

function Products() {
	let temp = useRef(0);
	
	const [openModal, setOpenModal] = useState(false);

	const classes = useStyles();
	const dispatch = useDispatch();

	let productList = useSelector((state) => state.products.products);

	let searchValuePro = useSelector((state) => state.products.valueSearchProduct);
	productList = productList.filter((item) => (item.name).toLowerCase().indexOf(searchValuePro) > -1);
	
	let loading = useSelector((state) => state.products.loading);
	let backgroundEnable = useSelector((state) => state.products.backgroundEnable);
	
	const stateLoadCreate = useSelector((state) => state.products.loadCreate);
	const stateBackgroundCreate = useSelector((state) => state.products.backgroundCreate);

	const stateLoadDelete = useSelector((state) => state.products.loadDelete);
	const stateBackgroundDelete = useSelector((state) => state.products.backgroundDelete);

	const stateLoadUpdate = useSelector((state) => state.products.loadUpdate);
	const stateBackgroundUpdate = useSelector((state) => state.products.backgroundUpdate);

	const stateMessage = useSelector((state) => state.products.message);
	const stateMessageSuccess = useSelector((state) => state.products.messageSuccess);
	
	// firebase
	
	const getDispatchProducts = () => {
		dispatch (
			getProducts()
		)
	}
	//clearProduct
	const clearDispatchProducts = () => {
		dispatch (
			clearStateProduct()
		)
	}
	useEffect(() => {
		getDispatchProducts();
	}, []);

	useEffect(() => {
		return () => {
			clearDispatchProducts();
		};
	}, []);
	// submit save create
	const createProductDispatch = (product, category, image) => {
		dispatch (
			createProduct({
				product: product,
				category: category,
				image: image,
			})
		)
	}
	const handleSubmitCreateProduct = (product, category, image) => {
		//loading create
		createProductDispatch(product, category, image);
	}

	//openModal
	const onOpenCreate = () => {
		setOpenModal(true);
	};
	const onCloseModal = () => {
		setOpenModal(false);
	}	
	//delete
	const onDeleteProductId = (id, category_ProductId, imageId) => {
		console.log(imageId);
		deleteProductDispatch(id, category_ProductId, imageId);
	}

	const deleteProductDispatch = (productId, category_ProductId, imageId) => {
		dispatch (
			deleteProduct({
				productId: productId,
				category_ProductId: category_ProductId,
				imageId: imageId,
			})
		)
	}
	//update
	const onUpdateProductNew = (productId, product, category_product, imageProduct) => {
		dispatchUpdateProduct(productId, product, category_product, imageProduct);
	}
	const dispatchUpdateProduct = (productId, product, category_product, imageProduct) => {
		dispatch (
			updateProduct({
				productId: productId,
				product: product,
				category_product: category_product,
				image: imageProduct,
			})
		)
	}
	//search sort

	const onSortProduct = (sortByProduct) => {
		dispatch (
			sortStateProduct({
				sortByProduct: sortByProduct,
			})
		)
	}
	
	const onSearchProduct = (valueSearchProduct) => {
		dispatch (
			searchStateProduct({
				valueSearchProduct: valueSearchProduct,
			})
		)
	}
	
	const getDispatchLimit = (limitScroll) => {
		dispatch (
			stateLimit({
				limitScroll: limitScroll,
			})
		)
	}
	//scroll
	const myRef = useRef(0);

	window.addEventListener("scroll", (event) => {
		let scroll = window.scrollY;
		let limitScroll = Math.ceil(scroll/180 + 5);

		if (limitScroll > temp.current) {
			temp.current = limitScroll;
			if (!getDispatchLimit) return;
			if (myRef.current) {
				clearTimeout(myRef.current);
			}
			myRef.current = setTimeout(() => {
				getDispatchLimit(limitScroll);
			}, 500);
		}
	});
	
	useEffect(() => {
		return () => window.scroll = null;
	}, []);

	return (
		<>
			<SearchProduct onSortProduct={onSortProduct} onSearchProduct={onSearchProduct} />
			<Message 
				stateMessage={stateMessage} 
				stateMessageSuccess={stateMessageSuccess} 
			/>
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label="simple table">
					<TableHead >
						<TableRow>
							<TableCell className={classes.th_table} >STT</TableCell>
							<TableCell className={classes.th_table} >Tên</TableCell>
							<TableCell className={classes.th_table} align="right">Hình Ảnh</TableCell>
							<TableCell className={classes.th_table} align="right">Mô tả</TableCell>
							<TableCell className={classes.th_table} align="right">Giá</TableCell>
							<TableCell className={classes.th_table} align="right">Danh mục</TableCell>
							<TableCell className={classes.th_table} align="right">
								<button type="button" className="btn btn-success" onClick={() => onOpenCreate()}>Thêm</button>
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody >
						{productList.map((product, index) => (
							<Product 
								key={index}
								stt={index + 1}
								product={product} 
								onDeleteProductId={onDeleteProductId} 
								onUpdateProductNew={onUpdateProductNew}
							/>
						))}
					</TableBody>
				</Table>
			</TableContainer>
			<CreateProduct 
				openModal={openModal} 
				setOpenModal={setOpenModal} 
				onCloseModal={onCloseModal}
				handleSubmitCreateProduct={handleSubmitCreateProduct}
			/>
			{ loading === false ? <div className={classes.loading_table}><img src={loadingImg} style={{maxWidth: '200px',}} /></div> : <div></div> }
			{ stateLoadCreate === true ? <div className={classes.loading_table}><img src={loadingImg} style={{maxWidth: '200px',}} /></div> : <div></div> }
			{ stateLoadDelete === true ? <div className={classes.loading_table}><img src={loadingImg} style={{maxWidth: '200px',}} /></div> : <div></div> }
			{ stateLoadUpdate === true ? <div className={classes.loading_table}><img src={loadingImg} style={{maxWidth: '200px',}} /></div> : <div></div> }
      		{ backgroundEnable === false ? <div className="background-root"></div> : <div></div> }
      		{ stateBackgroundCreate === true ? <div className="background-root"></div> : <div></div> }
      		{ stateBackgroundDelete === true ? <div className="background-root"></div> : <div></div> }
      		{ stateBackgroundUpdate === true ? <div className="background-root"></div> : <div></div> }
		</>

	);
}

export default Products;
