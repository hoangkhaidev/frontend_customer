/* eslint-disable jsx-a11y/img-redundant-alt */
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles } from '@material-ui/styles';
import React, { useState } from 'react';
import ComClick from './ComClick/ComClick';
import UpdateProduct from './UpdateProduct/UpdateProduct';

const useStyles = makeStyles(() => ({
    table: {
        minWidth: 650,
    },
    th_table: {
        fontSize: '15px',
        textAlign: 'left'
    },
    img_style: {
        width: '150px',
        height: '150px',
        borderRadius: '50%'
    }

}));


function Product(props) {

    const classes = useStyles();
    const { product, onDeleteProductId, onUpdateProductNew, stt } = props;

    const onDeleteProduct = (id, categorys, imageId) => {
        console.log(imageId);
        onDeleteProductId(id, categorys, imageId);
    }
    const onClickUpdateProduct = (product, id) => {
        onOpenUpdate();
    }
    //update
    const [openModalUpdate, setOpenModalUpdate] = useState(false);

	const onOpenUpdate = () => {
		setOpenModalUpdate(true);
	};
	const onCloseModalUpdate = () => {
		setOpenModalUpdate(false);
	}	
    //update
	const submitUpdateProduct = (productUpdate, category_product, imageProduct) => {
        onUpdateProductNew(product.id, productUpdate, category_product, imageProduct);
	}
	//delete
    const onDeleteProductBefore = () => {
        onOpenComClick();
    }
    const [openComClick, setComClick] = useState(false);
    const onOpenComClick = () => {
        setComClick(true);
    }
    const onCloseComClick = () => {
        setComClick(false);
    }

    return (
        <>
            <TableRow>
                <TableCell className={classes.th_table} component="th" scope="row">{stt}</TableCell>
                <TableCell className={classes.th_table} align="right">{product.name}</TableCell>
                <TableCell className={classes.th_table} align="right">
                    <img src={(product.images.length) ? product.images.map((item) => ( item.thumbnail )) : 'https://cidrapbusiness.org/wp-content/uploads/2017/10/noimage.gif'} className={classes.img_style} alt="Image" />
                </TableCell>
                <TableCell className={classes.th_table} align="right">{product.description}</TableCell>
                <TableCell className={classes.th_table} align="right">{product.price}</TableCell>
                <TableCell className={classes.th_table} align="right">
                    {
                        (product.categorys.length) ? product.categorys.map((item) => ( item.name )) : ''
                    }
                </TableCell>
                <TableCell className={classes.th_table} align="right">
                    <button type="button" className="btn btn-primary" onClick={() => onClickUpdateProduct(product.id, product)} style={{ marginRight: '5px' }}>Sửa</button>
                    <button type="button" className="btn btn-danger" onClick={() => onDeleteProductBefore(product.id)}>Xóa</button>
                </TableCell>
            </TableRow>
            <UpdateProduct 
                product={product}
				openModalUpdate={openModalUpdate}
				setOpenModalUpdate={setOpenModalUpdate}
				onCloseModalUpdate={onCloseModalUpdate}
				submitUpdateProduct={submitUpdateProduct}
			/>
            <ComClick
                type="delete"
                productId={product.id}
                category_ProductId={ product.category_ProductId }
                imageId={ product.imageId }
                openComClick={openComClick}
                onCloseComClick={onCloseComClick}
                onDeleteProduct={onDeleteProduct}
			/>
        </>
    );
}

export default Product;