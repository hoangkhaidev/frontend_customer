/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Divider,
  TextField,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Select,
  Button
} from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import ComClick from '../ComClick/ComClick';
import { useDispatch, useSelector } from 'react-redux';
import { getCategory } from '../../../store/asyncActions/category.asyncAction';

const useStyles = makeStyles(() => ({
  root: {},
  dialogForm: {
    overflow: 'unset !important'
  },
  dialogContent: {
    overflow: 'unset !important',
  },
  subTitle: {
    textAlign: 'center'
  },
  text: {
    textAlign: 'center',
    marginTop: '10px',
    color: '#d9d9d9',
    fontSize: '14px'
  },
  img_style: {
      width: '100px',
      height: '100px',
      borderRadius: '50%'
  }

}));

const CreateProduct = (props) => {
  // eslint-disable-next-line
  const {
    openModal,
    onCloseModal,
    handleSubmitCreateProduct,
    ...rest
  } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const category = useSelector((state) => state.category.category);

  const [formState, setFormState] = useState({
      name: '',
      description : '',
      price : 0,
  });

  const [formCategory, setFormCategory] = useState({
      category : '',
  });

  // handleChange
  const handleChange = (event) => {
    let target = event.target;
  	let name = target.name;
  	let value = target.value;
    if (value) {
      setFormState({
        ...formState,
        [name]: value,
      });
    }
  }

  const handleChangeCategory = (event) => {
    let target = event.target;
  	let name = target.name;
  	let value = target.value;
    if (value) {
      setFormCategory({
        ...formCategory,
        [name]: value,
      })
    }
  }
  // file upload
  const [image, setImage] = useState('');
  const [loadingImg, setLoadingImg] = useState(false);
  
  const onChangeFile = async(e) => {
    const files = e.target.files;
    const data = new FormData();
    data.append('file', files[0]);
    // data.append('upload', 'image');
    setLoadingImg(true);
    const res = await fetch(
      'http://localhost:4001/product/upload_image',
      {
        method: 'POST',
        body: data
      }
    );
    const file = await res.json();
    setImage({thumbnail : res.url + '/' + file.thumbnail});
    setLoadingImg(false);
  }
 
  //com click
  const [openComClick, setComClick] = useState(false);
  const onOpenComClick = () => {
    setComClick(true);
  }
  const onCloseComClick = () => {
    setComClick(false);
  }
  //submit form add
  const [errors, setErrors] = useState({});

  const handleValidation = () => {
    let errors = {};
    let formIsValid = true;
    const re = /^[0-9\b]+$/;
    //Name
    if(!formState.name.trim()){
      formIsValid = false;
      errors.name = 'Name cannot be empty';
    }
    //description
    if(!formState.description.trim()){
      formIsValid = false;
      errors.description = 'Description cannot be empty';
    }
    //price
    if(!re.test(formState.price)){
      formIsValid = false;
      errors.price = 'Price only number';
    }
    if(!formState.price){
      formIsValid = false;
      errors.price = 'Price cannot be empty';
    }
    //category
    if(!formCategory.category){
      formIsValid = false;
      errors.category = 'Category cannot be empty';
    }
    //Image
    if(!image.thumbnail.trim()){
      formIsValid = false;
      errors.image = 'Image cannot be empty';
    }
    setErrors(errors);
    return formIsValid;
  };

  const handleSubmitCreateBefore = (formState, formCategory, image) => {
    if(handleValidation()){
      onOpenComClick();
    }
  }
  const handleSubmitCreate = async (product, category, image) => {
    onCloseModal();
    handleSubmitCreateProduct(product, category, image);
  }

  useEffect(() => {
    dispatch(getCategory());
  }, []);
  
  useEffect(() => {
    setFormState({
      name : '',
      description : '',
      price : 0,
    });
    setFormCategory({
      category : '',
    })
    setImage({
      thumbnail : '',
    })
    // eslint-disable-next-line
  }, [openModal]);

  const onClose = () => {
    setFormState({
      name : '',
      description : '',
      price : 0,
    });
    setFormCategory({
      category : '',
    })
    onCloseModal();
  }
  return (
    <>
      <Grid item sm={12} xs={12}>
        <Dialog
          aria-labelledby="form-dialog-title"
          className={classes.dialogForm}
          open={openModal}
        >
          <DialogTitle id="form-dialog-title">Create Product</DialogTitle>

          <Divider />

          <DialogContent className={classes.dialogContent}>
            <Grid container spacing={2}>
              <Grid item sm={12} xs={12}>
                <Grid container spacing={2}>
                  <Grid className={classes.name} item sm={6} xs={6}>
                    <TextField
                      fullWidth
                      label="Name"
                      name="name"
                      onChange={handleChange}
                      required
                      variant="outlined"
                      margin="dense"
                    />
                    <span style={{color: "red"}}>{errors.name}</span>
                  </Grid>
                  <Grid className={classes.description} item sm={6} xs={6}>
                    <TextField
                      fullWidth
                      label="description"
                      name="description"
                      onChange={handleChange}
                      required
                      variant="outlined"
                      margin="dense"
                    />
                    <span style={{color: "red"}}>{errors.description}</span>
                  </Grid>
                  <Grid className={classes.price} item sm={6} xs={6}>
                    <TextField
                      fullWidth
                      label="price"
                      name="price"
                      onChange={handleChange}
                      required
                      variant="outlined"
                      margin="dense"
                    />
                    <span style={{color: "red"}}>{errors.price}</span>
                  </Grid>
                  <Grid className={classes.category} item sm={6} xs={6}>
                    <InputLabel id="label">Danh mục</InputLabel>
                    <Select labelId="label" name="category" style={{display: 'flex',}} id="select" value={formCategory.category} onChange={handleChangeCategory}>
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {
                          category.map((item, index) => (
                            <MenuItem key={index} value={item.id}>{item.name}</MenuItem>
                          ))
                        }
                    </Select>
                    <span style={{color: "red"}}>{errors.category}</span>
                  </Grid>
                  <Grid className={classes.image} item sm={6} xs={12} >
                    <Button
                      color="primary"
                      component="label"
                      variant="contained"
                      >
                      Upload Avatar
                      <input
                        name="image"
                        onChange={onChangeFile}
                        id="customFile"
                        type="file"
                        accept=".png, .jpg, .gif"
                      />
                    </Button>
                    { loadingImg ? (
                        <h3>Loading...</h3>
                      ) : (
                        <img src={image.thumbnail ? image.thumbnail : 'https://cidrapbusiness.org/wp-content/uploads/2017/10/noimage.gif'} className={classes.img_style} alt="Image" />
                      )
                    }
                    <span style={{color: "red"}}>{errors.image}</span>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>

          <Divider />

          <DialogActions>
            
            <button
              content="Create new"
              type="blue-full"
              color="primary"
              className={classes.button}
              variant="contained"
              onClick={() => handleSubmitCreateBefore(formState, formCategory, image) }
            >
              Save
            </button>
            <button
              content="Cancel"
              type="gray-full"
              variant="contained"
              color="secondary"
              className={classes.button}
              onClick={() => onClose()}
            >
              Cancel
            </button>
          </DialogActions>
        </Dialog>
      </Grid>
      <ComClick
        type="create"
        formState={formState}
        formCategory={formCategory}
        image={image}
				openComClick={openComClick}
				onCloseComClick={onCloseComClick}
        handleSubmitCreate={handleSubmitCreate}
			/>
    </>
  );
};

export default CreateProduct;

CreateProduct.propTypes = {
  openModal: PropTypes.bool,
  onCloseModal: PropTypes.bool,
  handleSubmitCreateProduct: PropTypes.func
};
