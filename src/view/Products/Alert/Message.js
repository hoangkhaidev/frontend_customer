/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import Collapse from '@material-ui/core/Collapse';
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

const Message = (props) => {
  // eslint-disable-next-line
  const {stateMessage, stateMessageSuccess} = props;
  const classes = useStyles();
  const [openMessage, setOpenMessage] = useState(false);
  useEffect(() => {
		setOpenMessage(stateMessage); 
	}, [stateMessage, stateMessageSuccess]);

  if (openMessage === true) {
    setTimeout(() => { 
      setOpenMessage(false); 
    }, 3000);
  }
  
  return (
    <div className={classes.root}>
      <Collapse in={openMessage}>
        <Alert severity="success">{stateMessageSuccess}</Alert>
      </Collapse>
    </div>
  );
};

export default Message;

