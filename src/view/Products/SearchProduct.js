/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { useRef, useState } from 'react';
import { fade, makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        search: {
            background : '#fff!important',
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginRight: theme.spacing(2),
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
            },
        },
        searchIcon: {
            padding: theme.spacing(0, 2),
            height: '100%',
            position: 'absolute',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            right : '0',
            cursor : 'pointer'
        },
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
        dflex: {
            display: 'flex',
            background: '#1976d2',
            padding : '10px',
            alignItems : 'center'
        },
    }),
);
function SearchProduct(props) {
    const classes = useStyles();
    const { onSortProduct, onSearchProduct } = props;
    const typingTimeoutRef = useRef(null);
    const onChange = (sortByProduct) => {
        onSortProduct(sortByProduct);
    }
    const [searchProductState, setSearchProductState] = useState('');
    const onHandleSearchProductChange = (event) => {
        let valueSearchProduct = event.target.value;

        setSearchProductState(valueSearchProduct);

        if (!onSearchProduct) return;

        if (typingTimeoutRef.current) {
            clearTimeout(typingTimeoutRef.current);
        }

        typingTimeoutRef.current = setTimeout(() => {
            onSearchProduct(valueSearchProduct);
        }, 500);
    }

    return (
            <div className={classes.dflex}>
                <Grid item xs={6}>
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder="Search…"
                            name={searchProductState}
                            onChange={ onHandleSearchProductChange }
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                </Grid>
                <Grid item xs={6}>
                    <div className="dropdown ">
                        <button className="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sắp Xếp 
                            <span  className="fa fa-caret-square-o-down ml-5"></span>
                        </button>
                        <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li onClick={ () => onChange(1) }>
                                <span className="fa fa-sort-alpha-asc pr-5">Tên A-Z</span>
                            </li>
                            <li onClick={ () => onChange(-1) } >
                                <span className="fa fa-sort-alpha-desc pr-5">Tên Z-A</span>
                            </li>
                        </ul>
                    </div>
                </Grid>
            </div>
    );
}

export default SearchProduct;