/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Divider,
  TextField,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Select,
  Button
} from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import ComClick from '../ComClick/ComClick';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(() => ({
  root: {},
  dialogForm: {
    overflow: 'unset !important'
  },
  dialogContent: {
    overflow: 'unset !important',
  },
  subTitle: {
    textAlign: 'center'
  },
  text: {
    textAlign: 'center',
    marginTop: '10px',
    color: '#d9d9d9',
    fontSize: '14px'
  },
  img_style: {
    width: '100px',
    height: '100px',
    borderRadius: '50%'
  }
}));

const UpdateProduct = (props) => {
  
  // eslint-disable-next-line
  const {
    openModalUpdate,
    onCloseModalUpdate,
    submitUpdateProduct,
    product,
    ...rest
  } = props;

  const classes = useStyles();
  const category = useSelector((state) => state.category.category);

  const [formState, setFormState] = useState({
    name: '',
    description : '',
    price : 0,
  });

  const [formCategory, setFormCategory] =  useState({
    categorys : [],
    category_ProductId : [],
  });

  const onCloseUpdate = () => {
    setFormState({
      name: '',
      description : '',
      price : 0,
    });
    setFormCategory({
      categorys : [],
      category_ProductId : [],
    });
    onCloseModalUpdate();
  }

  //handleChange
  const handleChange = (event) => {
    let target = event.target;
  	let name = target.name;
  	let value = target.value; 
    setFormState({
      ...formState,
      [name]: value,
    })
  }
  const [loadingImg, setLoadingImg] = useState(false);

  //changeFile
  const [image, setImage] = useState({
    images: [],
    imageId: []
  });

  const handleChangeFile = async(e) => {
    const files = e.target.files;
    const data = new FormData();
    data.append('file', files[0]);
    const res = await fetch(
      'http://localhost:4001/product/upload_image',
      {
        method: 'POST',
        body: data
      }
    );
    const file = await res.json();
    setImage({
      images : res.url + '/' + file.thumbnail,
      imageId : product.images ? product.images.map((item) => item.id) : '',
    });
  }

  const handleChangeCategory = (event) => {
    let target = event.target;
  	let name = target.name;
  	let value = target.value; 
    setFormCategory({
      ...formCategory,
      [name]: value,
    })
  }
  //submit update
  const [openComClick, setComClick] = useState(false);
  const onOpenComClick = () => {
    setComClick(true);
  }
  const onCloseComClick = () => {
    setComClick(false);
  }
  //submit form add
  const [errors, setErrors] = useState({});

  const handleValidation = () => {
    let errors = {};
    let formIsValid = true;
    console.log(formCategory);
    const re = /^[0-9\b]+$/;
    //Name
    if(!formState.name.trim()){
      formIsValid = false;
      errors.name = 'Name cannot be empty';
    }
    //description
    if(!formState.description.trim()){
      formIsValid = false;
      errors.description = 'Description cannot be empty';
    }
    //price
    if(!re.test(formState.price)){
      formIsValid = false;
      errors.price = 'Price only number';
    }
    if(!formState.price){
      formIsValid = false;
      errors.price = 'Price cannot be empty';
    }
    //category
    if(!formCategory.categorys){
      formIsValid = false;
      errors.category = 'Category cannot be empty';
    }
    setErrors(errors);
    return formIsValid;
  };

  const handleSubmitUpdateBefore = (formState) => {
    if(handleValidation()){
      onOpenComClick();
    }
  }
  const submitUpdate = async (dataProduct, category_product, imageProduct) => {
    //submit update
    onCloseModalUpdate();
    submitUpdateProduct(dataProduct, category_product, imageProduct);
  }

  useEffect(() => {
    setFormState({
      name : product.name,
      description : product.description,
      price : product.price,
    });
    setFormCategory({
      categorys : product.categorys ? product.categorys.map((item) => ( item.id )) : '',
      category_ProductId : product.category_ProductId,
    });
    setImage({
      images: product.images ? product.images.map((item) => item.thumbnail) : '',
      imageId: product.images ? product.images.map((item) => item.id) : '',
    })
  }, [product, openModalUpdate]);
  return (
    <>
      <Grid item sm={12} xs={12}>
        <Dialog
          aria-labelledby="form-dialog-title"
          className={classes.dialogForm}
          open={openModalUpdate}
        >
          <DialogTitle id="form-dialog-title">Update Product</DialogTitle>

          <Divider />

          <DialogContent className={classes.dialogContent}>
            <Grid container spacing={2}>
              <Grid item sm={12} xs={12}>
                <Grid container spacing={2}>
                  <Grid className={classes.name} item sm={6} xs={6}>
                    <TextField
                      fullWidth
                      label="Full Name"
                      name="name"
                      onChange={handleChange}
                      value={formState.name}
                      required
                      variant="outlined"
                      margin="dense"
                    />
                    <span style={{color: "red"}}>{errors.name}</span>
                  </Grid>
                  <Grid className={classes.description} item sm={6} xs={6}>
                    <TextField
                      fullWidth
                      label="description"
                      name="description"
                      onChange={handleChange}
                      value={formState.description}
                      required
                      variant="outlined"
                      margin="dense"
                    />
                    <span style={{color: "red"}}>{errors.description}</span>
                  </Grid>
                  <Grid className={classes.price} item sm={6} xs={6}>
                    <TextField
                      fullWidth
                      label="price"
                      name="price"
                      onChange={handleChange}
                      value={formState.price}
                      required
                      variant="outlined"
                      margin="dense"
                    />
                    <span style={{color: "red"}}>{errors.price}</span>
                  </Grid>
                  <Grid className={classes.category} item sm={6} xs={6}>
                    <InputLabel id="label">Danh mục</InputLabel>
                    <Select 
                      labelId="label" 
                      name="categorys" 
                      id="select" 
                      value={ formCategory.categorys }  
                      onChange={handleChangeCategory}
                      style={{display: 'flex',}}
                    >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {
                          category.map((item, index) => (
                            <MenuItem key={index} value={item.id}>{item.name}</MenuItem>
                          ))
                        }
                    </Select>
                    <span style={{color: "red"}}>{errors.category}</span>
                  </Grid>
                  <Grid className={classes.image} item sm={6} xs={12} >
                    <Button
                      color="primary"
                      component="label"
                      variant="contained"
                      >
                      Upload Avatar
                      <input
                        name="image"
                        type="file"
                        onChange={handleChangeFile}
                        accept=".png, .jpg, .gif"
                      />
                    </Button>
                    <img 
                      src={image.images.length ? image.images : 'https://cidrapbusiness.org/wp-content/uploads/2017/10/noimage.gif' } 
                      className={classes.img_style} 
                      alt="Image" 
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
          <Divider />

          <DialogActions>
            <button
              content="Update Product"
              type="blue-full"
              color="primary"
              className={classes.button}
              variant="contained"
              onClick={() => handleSubmitUpdateBefore(formState) }
            >
              Save
            </button>
            <button
              content="Cancel"
              type="gray-full"
              variant="contained"
              color="secondary"
              className={classes.button}
              onClick={() => onCloseUpdate()}
            >
              Cancel
            </button>
          </DialogActions>
        </Dialog>
      </Grid>
      <ComClick
        type="update"
        formState={formState}
        formCategory={formCategory}
        image={image}
				openComClick={openComClick}
				onCloseComClick={onCloseComClick}
        submitUpdate={submitUpdate}
			/>
    </>
  );
};

export default UpdateProduct;

UpdateProduct.propTypes = {
  openModalUpdate: PropTypes.bool,
  onCloseModalUpdate: PropTypes.func,
  submitUpdateProduct: PropTypes.func
};