/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { useSelector } from 'react-redux';
import App from '../../App';
import Login from '../Login/Login';
import Message from '../Message/message';

function Auth() {
  const token  = JSON.parse(localStorage.getItem('accessToken'));
  const isLogin = useSelector((state) => state.session.isSignIn);
  console.log(isLogin);
  return (
    <>
        { token ? <App token={token} /> : <Login />}
        <Message />
    </>
  );
}

export default Auth;