/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { SnackbarProvider, useSnackbar } from 'notistack';
import { useSelector } from 'react-redux';

function Message() {
  const { enqueueSnackbar } = useSnackbar();

  let message = useSelector((state) => state.session.message);
  let error = useSelector((state) => state.session.error);

  useEffect(() => {
    if (message) enqueueSnackbar(message, { variant: "success" });
    if (error) enqueueSnackbar(error, { variant: "error" });
	}, [message, error]);

  return (
    <React.Fragment>
    </React.Fragment>
  );
}

export default function IntegrationNotistack() {
  return (
    <SnackbarProvider maxSnack={3}>
      <Message />
    </SnackbarProvider>
  );
}