import React, { useEffect, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import validate from 'validate.js';
import { useDispatch } from 'react-redux';
import { signIn } from '../../store/asyncActions/session.asyncAction';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
const schema = {
    username: {
      presence: {allowEmpty: false, message: 'is required'}
    },
    password: {
      presence: {allowEmpty: false, message: 'is required'}
    }
}
export default function Login() {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [login, setLogin] = useState({
        isValid: false,
        values: {
            username: '',
            password: ''
        },
        errors: {},
        touched: {}
    });

    const handleChange = (event) => {
        event.persist();

        setLogin((login) => ({
            ...login,
            values: {
                ...login.values,
                [event.target.name]:
                event.target.type === 'checkbox'
                    ? event.target.checked
                    : event.target.value
            },
            touched: {
                ...login.touched,
                [event.target.name]: true
            }
        }));
    }
    const handleSubmit = (event) => {
        event.preventDefault();
        if (login.isValid === true) {
            loginDispatch(login.values);
        }
    }

    const loginDispatch = (user) => {
		dispatch(
			signIn({
				user
			})
		)
	}

    const hasError = (field) => login.touched[field] && login.errors[field] ? true : false;
    
    useEffect(() => {
      setLogin({
        isValid: false,
        values: {
            username: '',
            password: ''
        },
        errors: {},
        touched: {}
      });
      // eslint-disable-next-line
    }, []);
  
    useEffect(() => {
      const errors = validate(login.values, schema);
  
      setLogin((login) => ({
        ...login,
        isValid: errors ? false : true,
        errors: errors || {}
      }));
    }, [login.values]);

    return (
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
            <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
            Sign in
            </Typography>
            <form className={classes.form} noValidate onSubmit={handleSubmit}>
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="User name"
                name="username"
                error={hasError('username')}
                helperText={
                    hasError('username') ? login.errors.username[0] : null
                }
                onChange={handleChange}
                autoComplete="username"
                autoFocus
            />
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                error={hasError('password')}
                helperText={
                    hasError('password') ? login.errors.password[0] : null
                }
                onChange={handleChange}
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
            />
            <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={!login.isValid}
                className={clsx(classes.submit)}
            >
                Sign In
            </Button>
            <Grid container>
                <Grid item xs>
                <Link href="#" variant="body2">
                    Forgot password?
                </Link>
                </Grid>
                <Grid item>
                <Link href="#" variant="body2">
                    {"Don't have an account? Sign Up"}
                </Link>
                </Grid>
            </Grid>
            </form>
        </div>
        </Container>
    );
}