/* eslint-disable no-unused-vars */
import { createSlice } from '@reduxjs/toolkit';
import {
  getOrder,
  deleteOrder,
  getOrderFirst
} from '../asyncActions/order.asyncAction';

const orderSlice = createSlice({
  name: 'order',
  initialState: {
    order: [],
    searchValue: '',
    orderFirst: [],
  },
  reducers: {
    clearStateOrder: (state, action) => {
      state.order = [];
      state.searchValue = '';
    }
  },
  extraReducers: {
    //* get order
    [getOrder.pending]: (state, action) => {
    },
    [getOrder.fulfilled]: (state, action) => {
      if (action.payload) {
        state.order = action.payload;
      }
    },
    [getOrder.rejected]: (state, action) => {
    },

    [getOrderFirst.pending]: (state, action) => {
    },
    [getOrderFirst.fulfilled]: (state, action) => {
      if (action.payload) {
        state.orderFirst = action.payload;
      }
    },
    [getOrderFirst.rejected]: (state, action) => {
    },

    //delete
    [deleteOrder.pending]: (state, action) => {
    },
    [deleteOrder.fulfilled]: (state, action) => {
      if (action.payload){
        let index = state.order.map((item) => item.id).indexOf(action.payload);
        state.order.splice(index, 1);
      }
    },
    [deleteOrder.rejected]: (state, action) => {
    }
  }
});

const { actions, reducer } = orderSlice;

const { clearStateOrder } = actions;

export { clearStateOrder };

export default reducer;
