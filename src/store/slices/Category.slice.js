/* eslint-disable no-unused-vars */
import { createSlice } from '@reduxjs/toolkit';
import {
  createCategory,
  getCategory,
  updateCategory,
  deleteCategory
} from '../asyncActions/category.asyncAction';

const categorySlice = createSlice({
  name: 'category',
  initialState: {
    category: [],
    searchValue: '',
  },
  reducers: {
    clearStateCategory: (state, action) => {
      state.category = [];
      state.sortBy = '';
      state.searchValue = '';
    },
    mySort: (state, action) => {
      let sort = action.payload.sortBy; 
      if(sort){
        state.category = state.category.sort((a, b)=>{
          if(a.name > b.name){
            return sort;
          }else if(a.name < b.name){
            return -sort;
          }else{
            return 0;
          }
        });
      }

    },
    mySearch: (state, action) => {
      state.searchValue = action.payload.searchValue.toUpperCase();
    }
  },
  extraReducers: {
    //* get category
    [getCategory.pending]: (state, action) => {
    },
    [getCategory.fulfilled]: (state, action) => {
      if (action.payload) {
        state.category = action.payload;
      }
    },
    [getCategory.rejected]: (state, action) => {
    },

    // create
    [createCategory.pending]: (state, action) => {
    },
    [createCategory.fulfilled]: (state, action) => {
      if (action.payload) {
        state.category = [ action.payload, ...state.category ];
      }
    },
    [createCategory.rejected]: (state, action) => {
        console.log('sai');
    },

    //delete
    [deleteCategory.pending]: (state, action) => {
    },
    [deleteCategory.fulfilled]: (state, action) => {
      if (action.payload){
        let index = state.category.map((item) => item.id).indexOf(action.payload);
        state.category.splice(index, 1);
      }
    },
    [deleteCategory.rejected]: (state, action) => {
    },

    //update
    [updateCategory.pending]: (state, action) => {
    },
    [updateCategory.fulfilled]: (state, action) => {
      if (action.payload) {
        let index = state.category.map((item) => item.id).indexOf(action.payload.id);
        state.category[index] = {
          ...state.category[index],
          ...action.payload
        };
      }
    },
    [updateCategory.rejected]: (state, action) => {
    },
  }
});

const { actions, reducer } = categorySlice;

const { clearStateCategory, mySort, mySearch } = actions;

export { clearStateCategory, mySort, mySearch };

export default reducer;
