import { createSlice } from '@reduxjs/toolkit';
import { signIn } from '../asyncActions/session.asyncAction';

//*reducer handle
const sessionSlice = createSlice({
  name: 'session',
  initialState: {
    status: null,
    error: null,
    message: null,
    isSignIn: null,
    user: {
      permissions: [],
      avatar: ''
    }
  },
  reducers: {
    // eslint-disable-next-line no-unused-vars
    clearStateSession: (state, action) => {
      state.status = null;
      state.error = null;
      state.message = null;
      state.isSignIn = false;
    },
    logout: (state) => {
      localStorage.removeItem("accessToken");
      state.isSignIn = null;
    }
  },
  extraReducers: {
    //* signIn
    [signIn.pending]: (state, action) => {
      state.error = null;
      state.message = null;
    },
    [signIn.fulfilled]: (state, action) => {
      if (action.payload) {
        localStorage.setItem('accessToken', JSON.stringify(action.payload.accessToken));
        state.message = "Đăng nhập thành công!";
        state.isSignIn = true;
      }
    },
    [signIn.rejected]: (state, action) => {
      state.isSignIn = false;
      state.error = action.payload.error.message;
    }
  }
});

const { actions, reducer } = sessionSlice;

const { clearStateSession, logout } = actions;

export { clearStateSession, logout };

export default reducer;
