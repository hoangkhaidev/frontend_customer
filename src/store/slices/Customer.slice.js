/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
import { createSlice } from '@reduxjs/toolkit';
import {
  createCustomer,
  getCustomers,
  updateCustomer,
  deleteCustomer
} from '../asyncActions/customer.asyncAction';

const customerSlice = createSlice({
  name: 'customers',
  initialState: {
    customers: [],
    searchValue: '',
  },
  reducers: {
    clearStateCustomer: (state, action) => {
      state.customers = [];
      state.sortBy = '';
      state.searchValue = '';
    },
    mySort: (state, action) => {
      let sort = action.payload.sortBy; 
      if(sort){
        state.customers = state.customers.sort((a, b)=>{
          if(a.name > b.name){
            return sort;
          }else if(a.name < b.name){
            return -sort;
          }else{
            return 0;
          }
        });
      }

    },
    mySearch: (state, action) => {
      state.searchValue = action.payload.searchValue.toUpperCase();
    }
  },
  extraReducers: {
    //* get customers
    [getCustomers.pending]: (state, action) => {
    },
    [getCustomers.fulfilled]: (state, action) => {
      if (action.payload) {
        state.customers = action.payload;
      }
    },
    [getCustomers.rejected]: (state, action) => {
    },

    // create
    [createCustomer.pending]: (state, action) => {
    },
    [createCustomer.fulfilled]: (state, action) => {
      if (action.payload) {
        state.customers = [ action.payload, ...state.customers ];
      }
    },
    [createCustomer.rejected]: (state, action) => {
        console.log('sai');
    },

    //delete
    [deleteCustomer.pending]: (state, action) => {
    },
    [deleteCustomer.fulfilled]: (state, action) => {
      if (action.payload){
        let index = state.customers.map((item) => item.id).indexOf(action.payload);
        state.customers.splice(index, 1);
      }
    },
    [deleteCustomer.rejected]: (state, action) => {
    },

    //update
    [updateCustomer.pending]: (state, action) => {
    },
    [updateCustomer.fulfilled]: (state, action) => {
      if (action.payload) {
        let index = state.customers.map((item) => item.id).indexOf(action.payload.id);
        state.customers[index] = {
          ...state.customers[index],
          ...action.payload
        };
      }
    },
    [updateCustomer.rejected]: (state, action) => {
    },
  }
});

const { actions, reducer } = customerSlice;

const { clearStateCustomer, mySort, mySearch } = actions;

export { clearStateCustomer, mySort, mySearch };

export default reducer;
