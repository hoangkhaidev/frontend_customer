import { createSlice, current } from '@reduxjs/toolkit';
import {
    createProduct,
    getProducts,
    updateProduct,
    deleteProduct,
} from '../asyncActions/product.asyncAction';

const productSlice = createSlice({
    name: 'products',
    initialState: {
        products: [],
        valueSearchProduct: '',
        loading: false,
        backgroundEnable: false,
        loadCreate: false,
        backgroundCreate: false,
        loadUpdate: false,
        backgroundUpdate: false,
        loadDelete: false,
        backgroundDelete: false,
        message: false,
        messageSuccess: null,
        limit: 7,
    },
    reducers: {
        clearStateProduct: (state, action) => {
            state.products = [];
        },
        sortStateProduct: (state, action) => {
            // state.sortBy = '';
            let sortEnable = action.payload.sortByProduct;
            state.products = state.products.sort((a, b) => {
                if (a.name > b.name) return sortEnable;
                else if (a.name < b.name) return -sortEnable;
                else return 0;
            });
        },
        searchStateProduct: (state, action) => {
            state.valueSearchProduct = action.payload.valueSearchProduct.toLowerCase().trim();
        },
        stateLimit: (state, action) => {
            // console.log(action);
            state.limit = action.payload.limitScroll;
        }
    },
    extraReducers: {
        //* get customers
        [getProducts.pending]: (state, action) => {
        },
        [getProducts.fulfilled]: (state, action) => {
            if (action.payload) {
                state.products = action.payload;
                state.loading = true;
                state.backgroundEnable = true;
            }
        },
        [getProducts.rejected]: (state, action) => {
        },

        // create
        [createProduct.pending]: (state, action) => {
            state.message = false;
        },
        [createProduct.fulfilled]: (state, action) => {
            if (action.payload) {
                state.products = [action.payload, ...state.products];
                state.loadCreate = false;
                state.backgroundCreate = false;
                state.message = true;
                state.messageSuccess = 'Tạo sản phẩm thành công!';
            }
        },
        [createProduct.rejected]: (state, action) => {
            console.log('sai');
        },

        //delete
        [deleteProduct.pending]: (state, action) => {
            state.message = false;
            state.loadDelete = true;
            state.backgroundDelete = true;
        },
        [deleteProduct.fulfilled]: (state, action) => {
            state.loadDelete = false;
            state.backgroundDelete = false;
            if (action.payload) {
                let index = state.products.map((item) => item.id).indexOf(action.payload.id);
                state.products.splice(index, 1);
                state.message = true;
                state.messageSuccess = 'Xóa thành công!';
            }
        },
        [deleteProduct.rejected]: (state, action) => {
            state.loadDelete = false;
            state.backgroundDelete = false;
        },

        //update
        [updateProduct.pending]: (state, action) => {
            state.message = false;
            state.loadUpdate = true;
            state.backgroundUpdate = true;
        },
        [updateProduct.fulfilled]: (state, action) => {
            state.loadUpdate = false;
            state.backgroundUpdate = false;
            state.message = true;
            state.messageSuccess = 'Cập nhật thành công!';
            if (action.payload) {
                let index = state.products.map((item) => item.id).indexOf(action.payload.id);
                console.log(action.payload.id);
                console.log('after', current(state.products[index]));

                state.products[index] = {
                    ...state.products[index], 
                    ...action.payload,
                };
                
            }
        },
        [updateProduct.rejected]: (state, action) => {
            state.loadUpdate = false;
            state.backgroundUpdate = false;
        },
    }
});

const { actions, reducer } = productSlice;

const { clearStateProduct, sortStateProduct, searchStateProduct, loadCreateProduct, loadUpdateProduct, loadDeleteProduct, stateLimit } = actions;

export { clearStateProduct, sortStateProduct, searchStateProduct, loadCreateProduct, loadUpdateProduct, loadDeleteProduct, stateLimit };

export default reducer;