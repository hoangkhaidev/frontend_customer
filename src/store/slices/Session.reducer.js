import SessionSlice from './Session.slice';

const sessionReducer = SessionSlice;

export default sessionReducer;
