// import customerSlice from 'store/slices/customerSlide';
import customerReducer from './slices/Customer.slice';
import productReducer from './slices/Product.slice';
import categoryReducer from './slices/Category.slice';
import orderReducer from './slices/Order.slice';
import cartReducer from './slices/Cart.slice';
import sessionReducer from './slices/Session.slice';

const rootReducer = {
  customers: customerReducer,
  products: productReducer,
  category: categoryReducer,
  order: orderReducer,
  cart: cartReducer,
  session: sessionReducer,
};

export default rootReducer;