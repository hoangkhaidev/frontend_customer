import { createAsyncThunk } from '@reduxjs/toolkit';
import { categoryApi } from './../../services/apis';
import apiStatus from './../../utils/apiStatus';

//*thunk action
const createCategory = createAsyncThunk(
  'category/createCategory',
  async (data, props) => {
    try {
      const response = await categoryApi.createCategory(
        {
          ...data.category,
          name: data.category.name ? data.category.name.trim() : '',
        }
      );
      const category = response;
      return category;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

const getCategory = createAsyncThunk(
  'category/getCategory',
  async (data, props) => {
    try {
      const response = await categoryApi.getCategory(data);
      const category = response;
      return category;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      console.log(payload);
      return props.rejectWithValue(payload);
    }
  }
);

const updateCategory = createAsyncThunk(
  'category/updateCategory',
  async (data, props) => {
    console.log(data);
    try {
      const response = await categoryApi.updateCategory(
        data.categoryID,
        {
          ...data.category,
          name: data.category.name ? data.category.name.trim() : '',
        }
      );
      console.log(response);
      const category = response;
      return category;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

const deleteCategory = createAsyncThunk(
  'category/deleteCategory',
  async (data, props) => {
    try {
      await categoryApi.deleteCategory(data.categoryID);
      const categoryId = data.categoryID;
      return categoryId;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

export {
  createCategory,
  getCategory,
  updateCategory,
  deleteCategory
};


