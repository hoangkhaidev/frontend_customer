import { createAsyncThunk } from '@reduxjs/toolkit';
import orderApi from '../../services/apis/order.api';

const getOrder = createAsyncThunk(
  'order/getOrder',
  async (data, props) => {
    try {
      const response = await orderApi.getOrder(data);
      const order = response;
      return order;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      console.log(payload);
      return props.rejectWithValue(payload);
    }
  }
);

const getOrderFirst = createAsyncThunk(
  'order/getOrderFirst',
  async (data, props) => {
    try {
      const response = await orderApi.getOrderFirst(data.requestId);
      return response;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      console.log(payload);
      return props.rejectWithValue(payload);
    }
  }
);

const deleteOrder = createAsyncThunk(
  'order/deleteOrder',
  async (data, props) => {
    try {
      await orderApi.deleteOrder(data.orderID);
      const orderId = data.orderID;
      return orderId;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

export {
  getOrder,
  deleteOrder,
  getOrderFirst
};


