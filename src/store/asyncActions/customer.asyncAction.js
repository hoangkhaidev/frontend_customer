import { createAsyncThunk } from '@reduxjs/toolkit';
import { customersApi } from './../../services/apis';
import apiStatus from './../../utils/apiStatus';

//*thunk action
const createCustomer = createAsyncThunk(
  'customers/createCustomer',
  async (data, props) => {
    try {
      console.log(data);
      const response = await customersApi.createCustomer(
        {
          ...data.customer,
          name: data.customer.name ? data.customer.name.trim() : '',
          tel: data.customer.tel ? data.customer.tel.trim() : '',
          address: data.customer.address ? data.customer.address.trim() : '',
        }
      );
      const customer = response;
      return customer;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

const getCustomers = createAsyncThunk(
  'customers/getCustomers',
  async (data, props) => {
    try {
      const response = await customersApi.getCustomers(data);
      const customers = response;
      return customers;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      console.log(payload);
      return props.rejectWithValue(payload);
    }
  }
);

const getCustomer = createAsyncThunk(
  'customers/getCustomer',
  async (data, props) => {
    try {
      const response = await customersApi.getCustomer(data.customerId);
      const customer = response.data;
      return customer;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

const updateCustomer = createAsyncThunk(
  'customers/updateCustomer',
  async (data, props) => {
    console.log(data);
    try {
      const response = await customersApi.updateCustomer(
        data.customerID,
        {
          ...data.customer,
          name: data.customer.name ? data.customer.name.trim() : '',
          tel: data.customer.tel ? data.customer.tel.trim() : '',
          address: data.customer.address ? data.customer.address.trim() : '',
        }
      );
      console.log(response);
      const customer = response;
      return customer;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

const deleteCustomer = createAsyncThunk(
  'customers/deleteCustomer',
  async (data, props) => {
    try {
      await customersApi.deleteCustomer(data.customerID);
      const customerId = data.customerID;
      return customerId;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

export {
  createCustomer,
  getCustomers,
  getCustomer,
  updateCustomer,
  deleteCustomer
};


