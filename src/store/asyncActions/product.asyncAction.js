import { createAsyncThunk } from '@reduxjs/toolkit';
import { productsApi } from './../../services/apis';
import apiStatus from './../../utils/apiStatus';

//*thunk action
const createProduct = createAsyncThunk(
  'products/createProduct',
  async (data, props) => {
    try {
        const responseProduct = await productsApi.createProduct(
          {
            ...data.product,
            name: data.product.name ? data.product.name.trim() : '',
            description: data.product.description ? data.product.description.trim() : '',
            price: Number(data.product.price),
          }
        );

        const responseProductCategory = await productsApi.createProductCategory(
          {
            productId: responseProduct.id,
            categoryId: [data.category.category],
          }
        );

        const responseImage = await productsApi.createImage(
          {
            productId: responseProduct.id,
            image: {thumbnail: data.image.thumbnail},
          }
        );

        const response = {
          ...responseProduct, 
          categorys: [responseProductCategory.category],
          images: [responseImage],
        };
        const product = response;
        return product;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

const getProducts = createAsyncThunk(
  'products/getProducts',
  async (data, props) => {
    const responseProducts = await productsApi.getProducts();
    const products = responseProducts;
    return products;
  }
);

const getProduct = createAsyncThunk(
  'products/getProduct',
  async (data, props) => {
    try {
      const response = await productsApi.getCustomer(data.productId);
      const product = response.data;
      return product;
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

const updateProduct = createAsyncThunk(
  'products/updateProduct',
  async (data, props) => {
    console.log(data);
    try {
      if (data.image.imageId.length !== 0) {
        const responseUpdateProduct = await productsApi.updateProduct(
          data.productId,
          {
            ...data.product,
            name: data.product.name ? data.product.name.trim() : '',
            description: data.product.description ? data.product.description.trim() : '',
            price: Number(data.product.price),
          }
        );
        const responseUpdateCategoryProduct = await productsApi.updateCategoryProduct(
          {
            category_ProductId: data.category_product.category_ProductId,
            categoryId: data.category_product.categorys,
            productId: data.productId,
          }
        );
        const responseImage = await productsApi.updateImage(
          {
            imageId: data.image.imageId,
            productId: data.productId,
            image: {thumbnail: data.image.images},
          }
        );
        const response = {
          ...responseUpdateProduct, 
          categorys: [responseUpdateCategoryProduct.category],
          category_ProductId: [responseUpdateCategoryProduct.id],
          images: [responseImage],
        };
        console.log(response);
        return response;

      } else {
        const responseUpdateProduct = await productsApi.updateProduct(
          data.productId,
          {
            ...data.product,
            name: data.product.name ? data.product.name.trim() : '',
            description: data.product.description ? data.product.description.trim() : '',
            price: Number(data.product.price),
          }
        );

        const responseUpdateCategoryProduct = await productsApi.updateCategoryProduct(
          {
            category_ProductId: data.category_product.category_ProductId,
            categoryId: data.category_product.categorys,
            productId: data.productId,
          }
        );

        const response = {
          ...responseUpdateProduct, 
          categorys: [responseUpdateCategoryProduct.category],
          category_ProductId: [responseUpdateCategoryProduct.id],
          images: [],
        };
        console.log(response);
        return response;
      }
    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

const deleteProduct = createAsyncThunk(
  'products/deleteProduct',
  async (data, props) => {
    try {
      console.log(data);
      if(data.imageId.length !== 0) {
        await productsApi.deleteCategoryProduct(data.category_ProductId);
        await productsApi.deleteImage(data.imageId);
        const responseProduct = await productsApi.deleteProduct(data.productId);
        const response =  responseProduct;
        console.log(response);
        return response;
      } else {
        await productsApi.deleteCategoryProduct(data.category_ProductId);
        const responseProduct = await productsApi.deleteProduct(data.productId);
        const response =  responseProduct;
        console.log(response);
        return response;
      }

    } catch (err) {
      const newError = { ...err };
      const payload = { error: newError.response.data };
      return props.rejectWithValue(payload);
    }
  }
);

export {
  createProduct,
  getProducts,
  getProduct,
  updateProduct,
  deleteProduct,
};


