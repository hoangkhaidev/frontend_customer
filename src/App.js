/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import './App.css';
import { BrowserRouter as Router, NavLink, Route, Switch, Redirect } from 'react-router-dom';
import OrderDetail from './view/Order/OrderDetail';
import Order from './view/Order/Order';
import Category from './view/Category/Category';
import Customers from './view/Customers/Customers';
import Products from './view/Products/Products';
import StoreProduct from './view/StoreProduct/StoreProduct';
import Home from './view/Home/Home';
import Login from './view/Login/Login';
import { useDispatch } from 'react-redux';
import { logout } from './store/slices/Session.slice';

const App = (props) => {
  
  const { token } = props; 
  const param = window.location.pathname; 
  var param_new = param.replace('/', "");
  const dispatch = useDispatch();

  const [active, setActive] = useState(param);

  const changeActive = (str) => {
    setActive(str);
  } 

  const onLogOut = () => {
    dispatch(
			logout()
		)
  }

  return (
    <>
        <Router>
          <div className="App">
            <nav className="navbar navbar-inverse">
              <ul className="nav navbar-nav">
                <li className={ param_new === '' ? 'active' : '' } onClick = { () => changeActive('') }>
                  <NavLink to="/">Home</NavLink>
                </li>
                <li className={ param_new === 'product' ? 'active' : '' } onClick = { () => changeActive('product') }>
                  <NavLink to="/product">Product</NavLink>
                </li>
                <li className={ param_new === 'customer' ? 'active' : '' } onClick = { () => changeActive('customer') } >
                  <NavLink to="/customer">Customer</NavLink>
                </li>
                <li className={ param_new === 'category' ? 'active' : '' } onClick = { () => changeActive('category') } >
                  <NavLink to="/category">Category</NavLink>
                </li>
                <li className={ param_new === 'order' ? 'active' : '' } onClick = { () => changeActive('order') } >
                  <NavLink to="/order">Order</NavLink>
                </li>
                <li className={ param_new === 'storeProduct' ? 'active' : '' } onClick = { () => changeActive('storeProduct') } >
                  <NavLink to="/storeProduct">Store Product</NavLink>
                </li>
                <li style={{position: 'absolute',right: 0}} onClick = { () => onLogOut() } >
                  <div style={{ color: '#fff', margin: '15px', cursor: 'pointer' }}>Log Out</div>
                </li>
              </ul>
            </nav>
            <Route path="/" exact={true} component={Home} />
            <Route path="/login" exact={true} component={Login}>
              { token ? <Redirect to="/" /> : '' }
            </Route>
            <Route path="/product" exact={true} component={Products} />
            <Route path="/customer" exact={true} component={Customers} />
            <Route path="/category" exact={true} component={Category} />
            <Route path="/order" exact={true} component={Order} />
            <Route path="/order/:id" component={OrderDetail} />
            <Route path="/storeProduct" component={StoreProduct} />
          </div>
        </Router>
    </>
  );
}
export default App;
